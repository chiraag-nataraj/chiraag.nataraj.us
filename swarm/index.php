<!DOCTYPE html>
<html>
  <head>
    <title>Swarm Robotics</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php
       $URL = $_SERVER["SERVER_NAME"];
       print '<base href="http://'.$URL.'" />'
    ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <div id="container">
        <h1>Swarm Robotics</h1>   
        <h2>The Project</h2>
        <p>
          <img alt="robots.jpg" src="swarm/robots.jpg" id="robots" class="swarm" />
          I worked on a swarm robotics project at the Autonomous Systems Laboratory at
          <a href="http://engineering.villanova.edu/cendac" target="_blank">
            Villanova University
          </a>
          over two summers. It was a lot of fun.
        </p>

        <h3>Overview</h3>

        <p>
          The basic idea is as follows. Let's suppose that three robots are trying to make it to the point (0,0). One robot starts at (200,0), another starts at (0,200), and the third starts at (200,200). We wanted to use an algorithm called Particle Swarm Optimization. It is an algorithm that takes into account the present position of the robot, the best position the robot has ever been relative to the target, and the best position
          <em>
            any
          </em>
          robot has been with respect to the target. The main challenge lies in implementing this algorithm.
        </p>
        <h3>Hardware and Software</h3> 
        <p>
          <img src="swarm/nxt.jpg" alt="nxt.jpg" id="nxt" class="swarm" />
          In order to do it, we used a Lego Mindstorms NXT kit with three motors, a touch sensor, and an ultrasonic sensor, and hardware parts. Each of the three motors was a servo motor with a built-in tachometer to record the motor rotation with accuracy of &plusmn;1&deg;. The touch sensor would let me know when the robot hit something, and the ultrasonic sensor was used to avoid obstacles in the first place as well as to measure distance. The PSO algorithm needs information to be communicated to each of the robots about their positions, so communication is a key component in the implementation. In order to communicate between the NXTs, I decided to use Bluetooth. Because the NXTs did not contain enough Bluetooth I/O ports, I decided to use the computer as an intermediate. On the software end, I decided to use Java, as that is the only true programming language I k_blank. This was fine on the computer, but what about the NXTs?  Clearly, the built-in NXT operating system isn't powerful enough to handle complicated algorithms. I looked around and found
          <a href="http://lejos.sourceforge.net/" target="_blank">
            LeJoS
          </a>
          , which implements a Java Runtime Environment on the Lego Mindstorms NXT platform. I thought - this is perfect for the task! But - little did I know the troubles I would get into.
        </p>
        <p>
          Now that I had all of the preliminaries out of the way, I could get to the actual work of implementing and testing the Particle Swarm Optimization algorithm on these robots.
        </p>
        <h3>Trials and Tribulations</h3>
        <p>
          <img alt="dongle.jpg" src="swarm/dongle.jpg" id="dongle" class="swarm" />
          The first hurdle was trying to figure out how to communicate between the NXTs and the computer. I knew I had to use Bluetooth, but I wan't sure how. Then, after many hours of looking through the documentation for the Bluetooth interface, I figured out how to use it and managed to not get errors on the Java end. However, the computer still wouldn't connect!  I finally found out it was Windows acting up, and I figured I would try Linux before trying to fix the obscure problem on Windows. As soon as I tried to connect, it worked!  Therefore, I decided to use my Linux laptop for the remainder of the project.
        </p>

        <p>
          <a href="swarm/flowchart.jpg" target="_blank">
            <img alt="flowchart.jpg" src="swarm/flowchart.jpg" id="flowchart" class="swarm" />
          </a>
          Next, I implemented the PSO algorithm (see
          <a href="swarm/flowchart.jpg">
            flowchart
          </a>
          ). Once I had done that, the next challenge was getting the three robots to communicate their data with the computer, which would compare the different values and send the appropriate values back to each robot. The main problem with Bluetooth is that if the receiver isn't tuned to receiving, there could be an I/O error and the whole program could abort. Therefore, I needed to find a way to make sure all the robots were initialized and were connected to at the same time. Also, I needed to make sure the robots were receiving when the computer sent them the values. Therefore, I set it up so that the robots waited for a signal from the computer before starting. This was to ensure that all of them were initialized at the same time. Then, I also set it up so that as soon as they sent their values to the computer, they got ready to receive the values. This ensured they did not miss any values that were broadcast.
        </p>
        <p>
          After all of this, I had to find some way to log the data. This proved a little tricky in the beginning, as I knew nothing about File I/O in Java, but I soon learned how to do it, and it was relatively easy to store the values received by the computer from each robot into a log file easily readable and importable into MATLAB for further processing.
        </p>
        <h2>The Team</h2>
        <p>
          I was mentored by Dr. B. Samanta, a research professor at Villanova and by Mark Woods, a senior Mechanical Engineering student. Dr. Samanta derived the algorithm and explained it to me. Mark helped with the MATLAB processing (before I knew much MATLAB). I researched and did all the leJOS programming including troubleshooting. There were many problems that we encountered. I and Sanjeev Reddy, (who was a senior at Radnor High School) were responsible for all hardware/software issues. Sanjeev was trying to use C and MS Windows. I have always been partial to Linux and was using Java. So we worked in parallel. However, after some time it became apparent that Windows wouldn't do the trick because it was so buggy especially with communication, so we had to go over to Linux. Nobody on the team except me knew Linux, so the hardware/software implementation ended up becoming my problem!
        </p>
        <h2>Results</h2>
        <p>
          The results were great and made the months of work seem worthwhile. I give below a couple of sample results. Here is a picture of the trace of the robot in a spiral to find the object.
        </p>
        <div class="results">
          <img alt="Graph of robot going in a spiral" src="swarm/spiral.jpg" id="spiral" />
        </div>
        <p>
          Here is a graph of PSO implemented on three robots (Nova1, Nova2, and Nova3) going towards the target avoiding obstacles and each other.
        </p>
        <div class="results">
          <img alt="Robots heading towards goal" src="swarm/paths.jpg" id="paths" />
        </div>
        <p>
          Here is a sample video showing one of the many successful runs.
        </p>
        <div class="results">
          <iframe src="http://www.youtube.com/embed/uBAgbjq4L5Q" id="video" frameborder="0" allowfullscreen>
          </iframe>
        </div>
        <p>
          We ended up writing an
          <a href="reports/swarm-report.pdf" target="_blank">
            ASEE conference paper
          </a>
          which was based largely on my report. Please feel free to contact me at
          <a href="mailto:chiraag.nataraj@gmail.com">
            chiraag.nataraj@gmail.com
          </a>
          for more information, and if you are interested in the computer code.<br />
        </p>
      </div>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
