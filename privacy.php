<!DOCTYPE html>
<?php include('hsts.php') ?>
<html lang=en>
  <head>
    <title>
      Privacy Policy
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="main.css" type="text/css" rel="stylesheet">
    <?php include('base.php') ?>
  </head>  
  <body>
    <?php include('navigation.php') ?>
    <div id="content">
      <h1>
        Privacy Policy
      </h1>
      My privacy policy is a very simple one.
      <ul class="a">
        <li><span>My website does not track users.</span></li>
        <li><span>I do not keep logs of visitors (no IP storing or anything like that).</span></li>
        <li><span><em>My webmaster may</em>, but I have no control over that.</span></li>
        <li><span>JavaScript is used only to display LaTeX code and embed Youtube videos. In the Youtube case, I enable enhanced privacy mode by using <code>https://youtube-nocookie.com</code>.</span></li>
        <li><span>No cookies are used on my website.</span></li>
      </ul>
      Now <em>there's</em> a privacy policy everyone can agree to!
      <hr />
      <?php include('footer.html') ?>
    </div>
  </body>
</html>
