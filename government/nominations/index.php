<!DOCTYPE html>
<?php include('../../hsts.php') ?>
<html lang=en>
  <head>
    <title>Nomination Tracker</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../../main.css" type="text/css" rel="stylesheet" />
    <?php include('../../base.php') ?>
  </head>
  <body>
    <?php include('../../navigation.php') ?>
    <div id="content">
      <h1>
	<a href="https://www.senate.gov/legislative/nom_cal_civ.htm" target="_blank">Nominations coming up</a>
      </h1>
      <?php $nom = simplexml_load_file("https://www.senate.gov/legislative/LIS/nominations/NomCivilianPendingCalendar.xml"); foreach ($nom->Nomination as $n) { print "<p><a href=\"https://congress.gov/nomination/115th-congress/".$n->NominationDisplayNumber['NominationNumber']."\" target=\"_blank\">".$n->ReportingDescription."</a></p>"; } ?>
      <h1>
	<a href="https://www.senate.gov/legislative/nom_confc.htm" target="_blank">Confirmed nominations</a>
      </h1>
      <p>
	Listed confirmations are within the last month — click on the heading to see the full list.
      </p>
      <?php $nom = simplexml_load_file("https://www.senate.gov/legislative/LIS/nominations/NomCivilianConfirmed.xml"); foreach ($nom->Nomination as $n) { if ((time() - (60*60*24*30)) < strtotime($n->ReportingStageDate)) { $done = false; print "<p><a href=\"https://congress.gov/nomination/115th-congress/".$n->NominationDisplayNumber['NominationNumber']."\" target=\"_blank\">".$n->ReportingDescription."</a> — "; $doc = new DOMDocument(); $doc->loadHTMLFile("https://congress.gov/nomination/115th-congress/".$n->NominationDisplayNumber['NominationNumber']); $tds = $doc->getElementsByTagName("td"); foreach ($tds as $td) { $cns = $td->childNodes; if (!$done) { foreach ($cns as $cn) { if ($cn->hasAttributes() and $cn->hasAttribute("href")) { print "<a href=\"".$cn->getAttribute("href")."\" target=\"_blank\"> Vote Result </a></p>"; $done = true; break; }}}}}} ?>
      <hr />
      <?php include('../../footer.html') ?>
    </div>
  </body>
</html>
