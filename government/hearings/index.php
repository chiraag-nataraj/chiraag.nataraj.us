<!DOCTYPE html>
<?php include('../../hsts.php') ?>
<html lang=en>
  <head>
    <title>Senate Hearings</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../../main.css" type="text/css" rel="stylesheet" />
    <?php include('../../base.php') ?>
  </head>
  <body>
    <?php include('../../navigation.php') ?>
    <div id="content">
      <h1>
	<a href="https://www.senate.gov/committees/committee_hearings.htm">Senate Hearings</a>
      </h1>
      <?php $nom = simplexml_load_file("https://www.senate.gov/general/committee_schedules/hearings.xml"); foreach ($nom->meeting as $m) { print "<p>".$m->date." — ".$m->matter." (".$m->committee.")</p>"; } ?>
      <hr />
      <?php include('../../footer.html') ?>
    </div>
  </body>
</html>
