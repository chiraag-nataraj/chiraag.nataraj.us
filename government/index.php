<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Up-to-Date Information About Trump &amp; Congress</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
	Nominations
      </h1>
      <p>
	Nominations have moved <a href="government/nominations/index.php">here</a>.
      </p>
      <h1>
	Hearings
      </h1>
      <p>
	Hearings have moved <a href="government/hearings/index.php">here</a>.
      </p>
      <h1>
	Executive Orders &amp; Memoranda
      </h1>
      <p>
	Executive Orders have moved <a href="government/executive_orders/index.php">here</a>.
      </p>
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
