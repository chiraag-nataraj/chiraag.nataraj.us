<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>
      Photos
    </title>
    <meta charset="utf-8">
    <link type="text/css" href="../main.css" rel="stylesheet"/>
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <?php
    $images = array();
    $captions = array();
    $handle = file('./database', FILE_IGNORE_NEW_LINES);
    $n = 0;
    foreach($handle as $line) {
      $exploded = explode(":", $line);
      $images[$n] = trim($exploded[0]);
      $captions[$n] = trim($exploded[1]);
      $n+=1;
    }
    sort($images);
    sort($captions);
    $max = count($images);
    $cols = 1;
    $rows = ($max - ($max % $cols)) / $cols;
    $rem = $max % $cols;
    print '<div id="content">';
    print '<div class="photosidebar">';
    for($i = 0; $i < $rows; $i++) {
      for($j = 0; $j < $cols; $j++) {
        $imgurl = strtr($images[$cols * $i + $j], array(' ' => '%20'));
        print '<a href="photos/'.$imgurl.'" target="_blank" type="image/jpeg">';
        print '<img src="photos/'.$imgurl.'" class="photosidebar" alt="'.$captions[$cols * $i + $j].'"/>';
        print '</a>';
        /* print '<div class="gallerycaption">'.$captions[$cols * $i + $j].'</div>';
	 * print '</div>';*/
      }
    }
    print '</div>';
    print '<hr />';
    include('../footer.html');
    print '</div>';
    ?>
  </body>
</html>
