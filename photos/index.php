<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>
      Photos
    </title>
    <meta charset="utf-8">
    <link type="text/css" href="../main.css" rel="stylesheet"/>
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <?php
    $images = array();
    $captions = array();
    $handle = opendir('.');
    while(false != ($entry = readdir($handle))) {
      if(strpos($entry, ".jpg") !== false) {
        $images[] = $entry;
        $captions[] = substr($entry, 0, -4);
      }
    }
    sort($images);
    sort($captions);
    $max = count($images);
    $cols = 3;
    $rows = ($max - ($max % $cols)) / $cols;
    $rem = $max % $cols;
    print '<div id="content">';
    print '<table class="gallery">';
    print '<tbody>';
    for($i = 0; $i < $rows; $i++) {
      print '<tr>';
      for($j = 0; $j < $cols; $j++) {
        $imgurl = strtr($images[$cols * $i + $j], array(' ' => '%20'));
        print '<td>';
        print '<div class="gallery">';
        print '<a href="photos/'.$imgurl.'" target="_blank" type="image/jpeg">';
        print '<img src="photos/'.$imgurl.'" class="photogallery" alt="'.$captions[$cols * $i + $j].'"/>';
        print '</a>';
        print '<div class="gallerycaption">'.$captions[$cols * $i + $j].'</div>';
        print '</div>';
        print '</td>';
      }
      print '</tr>';
    }
    print '<tr>';
    for($k = 0; $k < $rem; $k++) {
      $imgurl = strtr($images[$cols * $i + $k], array(' ' => '%20'));
      print '<td>';
      print '<div class="gallery">';
      print '<a href="photos/'.$imgurl.'" target="_blank" type="image/jpeg">';
      print '<img src="photos/'.$imgurl.'" class="photogallery" alt="'.$captions[$cols * $i + $k].'"/>';
      print '</a>';
      print '<div class="gallerycaption">'.$captions[$cols * $i + $k].'</div>';
      print '</div>';
      print '</td>';
    }
    for($k = $rem; $k < $cols; $k++)
    {
      print '<td></td>';
    }
    print '</tr>';
    print '</tbody>';
    print '</table>';
    print '<hr />';
    include('../footer.html');
    print '</div>';
    ?>
  </body>
</html>
