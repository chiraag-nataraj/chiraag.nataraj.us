<!DOCTYPE html>
<?php include('hsts.php') ?>
<html lang=en>
  <head>
    <?php
       print '<title>Sent!</title>';
       print '<link href="main.css" type="text/css" rel="stylesheet"/>';
       print '<meta http-equiv="REFRESH" content="2;url=feedback/index.php">';
    ?>
    <?php include('base.php') ?>
  </head>
  <body>
    <?php include('navigation.php') ?>
    <div id="content">
      <?php
         $my_email = 'chiraag@chiraagnataraj.us';
         $subject = 'Personal Feedback';
         $feedback = $_POST['feedback'];
         $message = wordwrap($feedback, 80);
         mail($my_email, $subject, $message);
         print "Message sent!"
      ?>
      <hr width="100%" align="center" />
      <?php include('footer.html') ?>
    </div>
  </body>
</html>
