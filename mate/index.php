<!DOCTYPE html>
<html>
  <head>
    <title>The MATE ROV Competition</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php
       $URL = $_SERVER["SERVER_NAME"];
       print '<base href="http://'.$URL.'" />'
    ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>Background</h1>
      <p class="a">
        When I was in 11th grade, I found out that our school did not participate in such engineering competitions as
        <a href="http://www.coe.drexel.edu/seaperch/" target="_blank">
          Sea Perch
        </a>
        and
        <a href="http://materover.org/" target="_blank">
          MATE
        </a>
        ROV competitions even though we have great students capable of succeeding in these competitions.  Therefore, I took the initiative and formed the Future Engineers club, which competed in the Sea Perch competition in 2010 and the MATE competition in 2011.  To this end, we formulated design plans concerning the robot in general as well as specific tasks.
      </p>
      <h1>The MATE ROV Competition</h1>
      <p class="a">
        Every year, the
        <a href="http://materover.org/" target="_blank">
          MATE ROV Competition
        </a>
        requires competitors to complete four tasks, usually related to one overarching mission or theme.  In 2011, the theme was cleaning up the oil spill.  In essence, we were doing in 15 minutes what it took BP a couple of months to do!  The tasks were as follows:
      </p>
      <ol class="a">
        <li><span>Remove the damaged riser pipe</span></li>
        <li><span>Cap the oil well</span></li>
        <li><span>Collect water samples and measure depth</span></li>
        <li><span>Collect biological samples</span></li>
      </ol>
      <p class="a">
        I will explain the four tasks below in more detail.
      </p>
      <h2>Remove the damaged riser pipe</h2>
      <p class="b">
        The point of this task is to simulate one of the many methods BP tried to stop the oil flow.  BP tried to install a cap on the well in the hope that it would stop the flow of oil.  However, a part of the pipe is damaged.  The task is to attach a line to an anchor point on the pipe, simulate cutting the pipe by removing a Velcro strip, and moving the "cut" portion of the pipe from the area (in preparation for the next task).  We must attempt this task before moving on to the next one.  This task is worth 70 points.
      </p>
      <h2>Cap the oil well</h2>
      <p class="b">
        This task simulates the "top kill" procedure, where mud is pumped into the wellhead to stop the flow of oil, and the wellhead is subsequently capped.  Our version of this procedure involves picking up a hose line from the floor and connecting it to the wellhead, turning a valve wheel to stop the "oil", and then inserting a cap onto the wellhead.  This is the most difficult part of the competition, worth 120 points.
      </p>
      <h2>Collect water samples and measure depth</h2>
      <p class="b">
        This task is like a postmortem.  That is, after the previous two tasks are complete, we need to make sure that it worked i.e. there should not be any oil in the water.  We will need to interpret a graph to determine the depth at which to sample the water, measure the depth <b>at</b> the sample site to make sure it's correct, collect a sample of water from that depth, and bring back that sample to the surface.  This task is worth 80 points.
      </p>
      <h2>Collect biological samples</h2>
      <p class="b">
        The oil spill has affected many different organisms.  The point of this task is to bring back organisms to figure out if/how the oil spill has affected them.  We will need to collect a sea cucumber, a glass sponge, and a Chaceon crab and return these to the surface.  This task is worth 30 points.
      </p>
      <h1>Our plan</h1>
      <a href="mate/plan.png" target="_blank">
        <img src="mate/plan.png" alt="Plan" class="plan" />
      </a>
      <h2>The robot</h2>
      <p class="c">
        We thought a lot about the design of the robot since we started planning in the beginning of the school year.  The main problem was motor placement.  We had some experience from the previous year's Sea Perch competition, where the upward thrust motor failed to bring the robot back up after it sank down to the floor of the pool.  Therefore, we weren't sure if we should just use one motor and hope it would bring us up from a 13 ft. pool.  We then thought about adding some kind of ballast system like those found on submarines.  However, we decided that it would be too complicated and that it would just add another point through which the whole robot could fail miserably.  Finally, we came up with a design with two forward motors, two upward thrust motors, and some weights on the bottom if needed.  This plan is shown to the left.
      </p>
      <h2>The tasks</h2>
      <p class="c">
        Because most of the tasks require some sort of gripping device, we designed a claw which will open, close, and rotate.  We could therefore use it for the first, second, and possibly fourth task.  We thought about making two clamps, of differing sizes, to cope with the size difference between the Velcro/Sea creatures and the valve wheel.
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
