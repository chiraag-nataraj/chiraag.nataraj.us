<!DOCTYPE html>
<?php include('hsts.php') ?>
<html lang=en>
  <head>
    <title>
      Chiraag Nataraj&rsquo;s (ಚಿರಾಗ್ ನಟರಾಜ್&rsquo;s) Web Page
    </title>
    <meta charset="utf-8" />
    <link href="main.css" type="text/css" rel="stylesheet" />
    <?php include('base.php') ?>
  </head>  
  <body>
    <?php include('navigation.php') ?>
    <div id="content">
      <div id="container">
        <div id="cn_photo">
          <img src="chiraag_photo.jpg" alt="Photo of Chiraag" class="cn_photo" />
        </div>
        <div id="home_main_content">
          <h1>
            Welcome to my website!
          </h1>
          You have reached the homepage of Chiraag Nataraj.  I am a graduate student at
          <a href="http://brown.edu" target="_blank" type="text/html">
            Brown University.
          </a>
        </div>
      </div>
      <div id="#spacer">
        <hr />
      </div>
      <h2>
        Research
      </h2>
      <ul class="a">
	<li>
	  <a href="reports/undergrad-thesis.pdf" target="_blank" type="applications/pdf">
	    Peeling studies as a proxy for understanding fracture mechanics and other interesting phenomena
	  </a>
	  &mdash;
	  <span>
	    My undergraduate thesis at <a href="http://caltech.edu" target="_blank">Caltech</a> with adviser Professor Kaushik Bhattacharya from 2014 to 2015. Peeling experiments and computer simulations are used to investigate fracture and crack propagation in heterogeneous materials.
	  </span>
	</li>
        <li>
          <a href="reports/kinect-report.pdf" target="_blank" type="applications/pdf">
            Monitoring Patient Motion Using Kinect Sensors to Optimize Spinal Cord Injury Therapy
          </a>
          &mdash;
          <span>
            A project I carried out at <a href="http://caltech.edu" target="_blank">Caltech</a> in the Burdick Lab during the summer of 2014. The key contribution of this research is in the automation of the initialization phase of the SCAPE algorithm to enable the utilization of depth sensor technology to track body motion. As part of the research team, I focused on implementing the new algorithm for the initialization, while a graduate student worked on implementing the SCAPE algorithm and calibrating the sensors.
          </span>
        </li>
        <li>
          <a href="reports/hydraulics-report.pdf" target="_blank" type="applications/pdf">
            Hydraulics for Micro-Robotic Actuation
          </a>
          &mdash;
          <span>
            A project I carried out at the Army Research Lab during the summer of 2013. Parameterized CAD drawings of the different parts of a pump for a robotic arm actuator are utilized in a programmatic manner to facilitate optimizing parameters. These CAD models are then utilized to 3D print a functioning pump in one piece.
          </span>
        </li>
        <li>
          <a href="reports/nims-report.pdf" target="_blank" type="applications/pdf">
            Modeling of NIMs for &#8220;Hide-In-Plain-Sight&#8221;
          </a>
          &mdash;
          <span>
            A project I carried out at the Army Research Lab during the summer of 2012. The possibility of creating &#8220;hide-in-plain-sight&#8221; effects using negative index metamaterials (NIMs) was explored with the goal of concealing or disguising an object. POV-Ray, a raytracing software, was used to model NIMs in various configurations, geometries, and gradients of index of refraction to study the effect on an observer's perception of an object enclosed in metamaterial.
          </span>
        </li>
        <li>
          <strong>
            Chaotic Dynamics
          </strong>
          &mdash;
          <span>
            Interpretation of nonlinear dynamic chaos through music, a project I carried out at
            <a href="http://www.navsea.navy.mil/nswc/carderock/default.aspx" target="_blank" type="text/html">
              Naval Surface Warfare Center,
            </a>
            Philadelphia, Summer 2010.
          </span>
          <ul>
            <li>
              <span>
                Here is a
                <a href="reports/chaos-report.pdf" target="_blank" type="application/pdf">
                  report
                </a>
                on the project; if you use it, please refer to it as
                <em>
                  Chiraag Nataraj, &#8220;Understanding Complexity in Nonlinear Dynamic Systems Using Musical Compositions,&#8221; Technical Report, Naval Surface Warfare Center, Philadelphia, PA, September 1, 2010.
                </em>
                An email to me would also be appreciated.
              </span>
            </li>
            <li>
              <span>
                Some of my music compositions arising out of chaotic oscillations!
              </span>
              <ul>
                <li>
                  <a href="chaos/chiraag_chaos_henon.mp3" target="_blank" type="audio/mpeg">
                    The Henon map
                  </a>
                </li>
                <li>
                  <a href="chaos/chiraag_chaos_logistic.mp3" target="_blank" type="audio/mpeg">
                    The Logistic map
                  </a>
                </li>
                <li>
                  <a href="chaos/chiraag_chuaMusikChaos_500_chord_instr.mp3"  target="_blank" type="audio/mpeg">
                    Chua circuit
                  </a>
                </li>
                <li>
                  <a href="chaos/chiraag_chaos_LorenzMusikChaos_438_chord_instr.mp3"  target="_blank" type="audio/mpeg">
                    Lorenz attractor
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <strong>
            Swarm Robotics
          </strong>
          &mdash;
          <span>
            A project I carried out at Autonomous Systems Laboratory,
            <a href="http://www.villanova.edu/engineering/research/centers/cendac/" target="_blank" type="text/html">
              Villanova University
            </a>
            during the summers of 2008 and 2009.  I worked with a research group; my responsibility was to get a group of NXT robots working in a swarm in autonomous fashion.  I did it with a version of Java.  A report that was eventually published as a paper at the 2010 Annual American Society of Engineering Education Conference is
            <a href="reports/swarm-report.pdf" target="_blank" type="application/pdf">
              here.
            </a>
            A video of a successful run can be found at various resolutions:
          </span>
	  <ul>
	    <li>
	      <a href="swarm/swarm-256x144.mp4" target="_blank" type="video/mp4">256x144</a>
	    </li>
	    <li>
	      <a href="swarm/swarm-426x240.mp4" target="_blank" type="video/mp4">426x240</a>
	    </li>
	    <li>
	      <a href="swarm/swarm-640x360.mp4" target="_blank" type="video/mp4">640x360</a>
	    </li>
	    <li>
	      <a href="swarm/swarm-854x480.mp4" target="_blank" type="video/mp4">854x480</a>
	    </li>
	    <li>
	      <a href="swarm/swarm-1280x720.mp4" target="_blank" type="video/mp4">1280x720</a>
	    </li>
	    <li>
	      <a href="swarm/swarm-1920x1080.mp4" target="_blank" type="video/mp4">1920x1080</a>
	    </li>
	  </ul>
        </li>
      </ul>
      <h2>
        Music
      </h2>
      <ul class="a">
        <li>
          <span>
            Tabla, the Indian percussion instrument.  My current guru and mentor is the renowned artist
            <a href="http://www.tabla.org" target="_blank" type="text/html">
              Pandit Samir Chatterjee
            </a>
            .  Tabla is arguably the most sophisticated "drum" instrument in the world with an extensive tonal range.  In fact, it is so versatile that it is not just used for accompaniment; often, a solo tabla concert can be very long and mesmerizingly entertaining.
          </span>
          <ul>
            <li>
              <span>
                I have been learning tabla since I was 7!  My big tabla concert was in October 2008 with
                <a href="http://www.shafaatullahkhan.com" target="_blank" type="text/html">
                  Ustad Shafaatullah Khan
                </a>
                on the sitar; it was held at Kimberton Waldorff High School. The concert was about an hour long; I have edited it to retain only 10 minutes of audio, which you can find
                <a href="kimberton_concert_excerpts.mp3" target="_blank" type="audio/mpeg">
                  here
                </a>
                .   A longer video is on
                <a href="videos/index.php" type="text/html">
                  Youtube.
                </a>
              </span>
            </li>
          </ul>
        </li>
        <li>
          <span>
            I have been into Indian classical and popular music for many years; I love to sing in Kannada (my mother tongue) and Hindi (so-called Bollywood songs).  I also learnt Western classical piano for many years and love to play my Yamaha keyboard.  Many Youtube links to all these music performances are on my <a href="videos/index.php">videos</a> page.
          </span>
        </li>
      </ul>
      <p>
        Please feel free to contact me at
        <a href="mailto:chiraag.nataraj@gmail.com">
          chiraag.nataraj@gmail.com.
        </a>
      </p>
      <hr />
      <?php include('footer.html') ?>
    </div>
  </body>
</html>
