<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>
      swara sAhitya sudhe
    </title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php include('../base.php') ?>
    <script src='videos/eventlisteners.js'></script>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        swara sAhitya sudhe
      </h1>
      <p>
        These are the links to the swara sAhitya sudhe (ಸ್ವರ ಸಾಹಿತ್ಯ ಸುಧೆ) bhAvagIte program performed at AKKA 2016. swara sAhitya sudhe was a literary music fusion program held at AKKA World Kannada Conference 2016, Atlantic City, USA and conceived and conducted by Smt. Prathibha Keshavamurthy. You can select an individual piece by clicking in the left hand column. If you have trouble playing these, you can also find them in YouTube by searching for "amerikannadiga swara sAhitya sudhe". Thanks for visiting!
      </p>
      <?php
      $nameurls = array(
	"hachchEvu kannaDada dIpa (ಹಚ್ಚೇವು ಕನ್ನಡದ ದೀಪ)" => array(null,"https://www.youtube-nocookie.com/embed/bTpRo3LEmio"),
	"nAku taMti (ನಾಕು ತಂತಿ)" => array("https://www.youtube-nocookie.com/embed/7yVCvOR1wAc","https://www.youtube-nocookie.com/embed/oAcH91FgdUQ"),
	"iLidu bA (ಇಳಿದು ಬಾ)" => array("https://www.youtube-nocookie.com/embed/eKB_zirCTJI","https://www.youtube-nocookie.com/embed/c2tKFiOclZs"),
	"uriva bisilirali (ಉರಿವ ಬಿಸಿಲಿರಲಿ)" => array("https://www.youtube-nocookie.com/embed/481NIOIwrIQ","https://www.youtube-nocookie.com/embed/MBFrZuGem44"),
	"nAnoMdu kanasa kaMDe (ನಾನೊಂದು ಕನಸ ಕಂಡೆ)" => array("https://www.youtube-nocookie.com/embed/3DdnAuoZnlI","https://www.youtube-nocookie.com/embed/qppk5uF44UA"),
	"kurigaLu sAr (ಕುರಿಗಳು ಸಾರ್)" => array("https://www.youtube-nocookie.com/embed/gwZoHxsDOQw","https://www.youtube-nocookie.com/embed/4WLKSGLxBWA"),
	"rekke iddare sAkE (ರೆಕ್ಕೆ ಇದ್ದರೆ ಸಾಕೇ)" => array("https://www.youtube-nocookie.com/embed/PnD_yeKRrAQ","https://www.youtube-nocookie.com/embed/8Kdt43lf3Gg"),
	"kODagana kOLi nuMgittA (ಕೋಡಗನ ಕೋಳಿ ನುಂಗಿತ್ತಾ)" => array("https://www.youtube-nocookie.com/embed/YZHFkRmcnK4","https://www.youtube-nocookie.com/embed/6I-QUZ73Ahk"),
	"bhAgyAda baLegAra (ಭಾಗ್ಯಾದ ಬಳೆಗಾರ)" => array("https://www.youtube-nocookie.com/embed/__Gzl5n4D-s","https://www.youtube-nocookie.com/embed/rvSNkZz-ZtY"),
	"ninnolumeyiMdalE (ನಿನ್ನೊಲುಮೆಯಿಂದಲೇ)" => array("https://www.youtube-nocookie.com/embed/jv7AT8mrWx4","https://www.youtube-nocookie.com/embed/HGbJXeQl1tQ"),
	"nI hiMge nODabyADa (ನೀ ಹಿಂಗೆ ನೊಡಬ್ಯಾಡ)" => array("https://www.youtube-nocookie.com/embed/VhJwB7dFui8","https://www.youtube-nocookie.com/embed/BiDXjdQEYhU"),
	"oMdiruLu kanasinali (ಒಂದಿರುಳು ಕನಸಿನಲಿ)" => array("https://www.youtube-nocookie.com/embed/yPPXriJv_go","https://www.youtube-nocookie.com/embed/8CXbOy0rPHE"),
	"bA savitA (ಬಾ ಸವಿತಾ)" => array("https://www.youtube-nocookie.com/embed/1jkLbkfkmvI","https://www.youtube-nocookie.com/embed/2WBBCtDDGUs"),
	"jaya bhArati (ಜಯ ಭಾರತಿ)" => array("https://www.youtube-nocookie.com/embed/PnLSDjxQvew","https://www.youtube-nocookie.com/embed/On5Y5wiMpeU")
      );
      print '<ul>';
      $k = 0;
      foreach($nameurls as $name => $url)
      {
	if($url[0] != null)
	{
	  print '<li>';
	  /* print '<label for="vid'.$k.'i" class="vidlist" onclick=\'return playVideoTest("vid'.$k.'i","videoPlayback'.$k.'i","'.$url[0].'")\'>'; */
	  print '<label for="vid'.$k.'i" class="vidlist" data-url="'.$url[0].'">';
	  print "Introduction for ".$name;
	  print '</label>';
	  print '<input type="checkbox" id="vid'.$k.'i"/>';
	  print '<div class="videoPlayback" id="videoPlayback'.$k.'i">';
	  print '<noscript>';
	  print '<iframe class="ytembed" src="'.$url[0].'?rel=0" allowfullscreen></iframe>';
	  print '</noscript>';
	  print '</div>';
	  print '</li>';
	}
	print '<li>';
	/* print '<label for="vid'.$k.'" class="vidlist" onclick=\'return playVideoTest("vid'.$k.'","videoPlayback'.$k.'","'.$url[1].'")\'>'; */
	print '<label for="vid'.$k.'" class="vidlist" data-url="'.$url[1].'">';
	print $name;
	print '</label>';
	print '<input type="checkbox" id="vid'.$k.'"/>';
	print '<div class="videoPlayback" id="videoPlayback'.$k.'">';
	print '<noscript>';
	print '<iframe class="ytembed" src="'.$url[1].'?rel=0" allowfullscreen></iframe>';
	print '</noscript>';
	print '</div>';
	print '</li>';
	$k += 1;
      }
      print '</ul>';
      ?>
      <p>
	<a href="videos/index.php" type="text/html">
          Back
        </a>
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
