<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>
      Miscellaneous Performances
    </title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php include('../base.php') ?>
    <script src='videos/eventlisteners.js'></script>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        Tabla Performances
      </h1>
      <p>
        These are the links to miscellaneous tabla performances (my big tabla concert is on my <a href="videos/rangapravesha.php">raMgapravEsha</a> page). If you have trouble playing these, you can also find them in YouTube by searching for "Chiraag Nataraj." Thanks for visiting!
      </p>
      <?php
      $nameurls = array(
	"Tabla for mom for kanasa kaMDEne (ಕನಸ ಕಂಡೇನೆ) (2010)" => "https://www.youtube-nocookie.com/embed/Kjt1ehEJWnY",
	"Tabla with Ustaad shafAtullAh khAn on sitar (2008)" => "https://www.youtube-nocookie.com/embed/P9z4n6s-Whg"
      );
      $disabled = array(
	"Classical piece, sAmAnyavalla (2010)" => "https://www.youtube-nocookie.com/embed/rjW8kHRWmvQ",
	"Kannada song, tharikere Eri mEe for AKKA (2010)" => "https://www.youtube-nocookie.com/embed/CQpN-ldEruc",
	"Kannada song, tharikere Eri mEe with Rochitha (2009)" => "https://www.youtube-nocookie.com/embed/tNGy67d3Hqo",
	"Kannada song, thEra Eri (2009)" => "https://www.youtube-nocookie.com/embed/xHka2A65q64"
      );
      print '<ul>';
      $k = 0;
      foreach($nameurls as $name => $url)
      {
	print '<li>';
	print '<label for="vid'.$k.'" class="vidlist" data-url="'.$url.'">';
	print $name;
	print '</label>';
	print '<input type="checkbox" id="vid'.$k.'" />';
	print '<div class="videoPlayback" id="videoPlayback'.$k.'">';
	print '<noscript>';
	print '<iframe class="ytembed" src="'.$url.'?rel=0" allowfullscreen></iframe>';
	print '</noscript>';
	print '</div>';
	print '</li>';
	$k += 1;
      }
      print '</ul>';
      ?>
      <p>
	<a href="videos/index.php" type="text/html">
	  Back
	</a>
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
