<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>
      raMgapravEsha Videos
    </title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php include('../base.php') ?>
    <script src='videos/eventlisteners.js'></script>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        raMgapravEsha Videos
      </h1>
      <p>
        These are the links to my rangapravesha (debut classical tabla concert) held on August 13, 2011 at Eastern University, PA.  You can select an individual piece by clicking in the left hand column.    You can find more from the links on my <a href="videos/index.html">video page</a>.  If you have trouble playing these, you can also find them in YouTube by searching for "Chiraag Nataraj debut."  Thanks for visiting!
      </p>
      <noscript>
	<p>Test this</p>
      </noscript>
      <?php
      $nameurls = array(
	"Solo tabla with Sri Neel Nadkarni" => "https://www.youtube-nocookie.com/embed/HU9Yuql365o",
	"With Smt. Deepal Chodhari on santoor" => "https://www.youtube-nocookie.com/embed/0EhrhWA3gwE",
	"With my mother singing nA ninnoLanya (ನಾ ನಿನ್ನೊಳನ್ಯ)" => "https://www.youtube-nocookie.com/embed/8seiqwV2xHc",
	"With my mother singing barase badariyA (बरसे बदरिया)" => "https://www.youtube-nocookie.com/embed/Yotbz1SDNKQ",
	"With my mother singing lOkA lOkE Om (ಲೋಕಾ ಲೋಕೇ ಓಂ)" => "https://www.youtube-nocookie.com/embed/Jv7Q0pbR9Ko"
      );
      print '<ul>';
      $k = 0;
      foreach($nameurls as $name => $url)
      {
	print '<li>';
	print '<label for="vid'.$k.'" class="vidlist" data-url="'.$url.'">';
	print $name;
	print '</label>';
	print '<input type="checkbox" id="vid'.$k.'" />';
	print '<div class="videoPlayback" id="videoPlayback'.$k.'">';
	print '<noscript>';
	print '<iframe class="ytembed" src="'.$url.'?rel=0" allowfullscreen></iframe>';
	print '</noscript>';
	print '</div>';
	print '</li>';
	$k += 1;
      }
      print '</ul>';
      ?>
      <p>
        <a href="videos/index.php" type="text/html">
          Back
        </a>
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
