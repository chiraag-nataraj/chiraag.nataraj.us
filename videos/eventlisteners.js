window.onload = function() {
    var x = document.getElementsByClassName("vidlist");
    for(var i = 0; i < x.length; i++) {
	var vid = x[i].attributes["for"].value;
	var idx = vid.slice(3);
	var url = x[i].attributes["data-url"].value;
	x[i].addEventListener(
	    'click',
	    function(vid,idx,url) {
		return function() {
		    var c = document.getElementById(vid).checked;
		    document.getElementById(vid).checked = !c;
		    document.getElementById(vid).click();
		    document.getElementById("videoPlayback" + idx.toString()).innerHTML = '<iframe class="ytembed" src="' + url + '?rel=0" allowfullscreen></iframe>';
		}
	    }(vid,idx,url));
    }
}
