<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>
      Vocal Performances
    </title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php include('../base.php') ?>
    <script src='videos/eventlisteners.js'></script>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        Vocal Performances
      </h1>
      <p>
        These are the links to my vocal performances. If you have trouble playing these, you can also find them in YouTube by searching for "Chiraag Nataraj." Thanks for visiting!
      </p>
      <?php
      $nameurls = array(
	"Samskrtam stOtra shiva tANDava stOtra (शिव ताण्डव स्तोत्र) (2017)" => "https://www.youtube-nocookie.com/embed/9-rQ1BtfSJA",
	"Kannada song akka pakka (ಅಕ್ಕ ಪಕ್ಕ) (2016)" => "https://www.youtube-nocookie.com/embed/l19Rz6XuZWk",
	"Kannada song tErA Eri (ತೇರಾ ಏರಿ) (2014)" => "https://www.youtube-nocookie.com/embed/FGxah7GXnvk",
	"Hindi song bhUlI hui yAdOM (भूली हुई यादों) (2014)" => "https://www.youtube-nocookie.com/embed/2x-Srnu3TUQ",
	"Hindi song jAne kahAM gaE wo din (जाने कहां गए वो दिन) (2014)" => "https://www.youtube-nocookie.com/embed/yl7SH20jQtI",
	"Hindi song oh re tAl mile (ओह रे ताल मिले) (2014)" => "https://www.youtube-nocookie.com/embed/2b_Ip7IJDRg",
	"Hindi song aji aisA maukA phir kahAM milegA (अजी ऐसा मौका फिर कहां मिलेगा) (2014)" => "https://www.youtube-nocookie.com/embed/AGEF0HVPgFw",
	"Hindi song O khaike pAn banAras wAlA (ऒ खाइके पान बनारस वाला) (2014)" => "https://www.youtube-nocookie.com/embed/Kd9v6mOChn0",
	"Hindi song churA liyA (चुरा लिया) (2012)" => "https://www.youtube-nocookie.com/embed/q1VOVrKgTT0",
	"Kannada song tarikere Eri mEle (ತರಿಕೆರೆ ಏರಿ ಮೇಲೆ) (2012)" => "https://www.youtube-nocookie.com/embed/CQpN-ldEruc",
	"Kannada song, kurigaLu sAr (ಕುರಿಗಳು ಸಾರ್) for AKKA (2010)" => "https://www.youtube-nocookie.com/embed/vicvDF_DnPY",
	"Kannada song, Ene subbi (ಏನೇ ಸುಬ್ಬಿ) with Rochitha (2009)" => "https://www.youtube-nocookie.com/embed/BmST9m3OwXc"
      );
      print '<ul>';
      $k = 0;
      foreach($nameurls as $name => $url)
      {
	print '<li>';
	print '<label for="vid'.$k.'" class="vidlist" data-url="'.$url.'">';
	print $name;
	print '</label>';
	print '<input type="checkbox" id="vid'.$k.'" />';
	print '<div class="videoPlayback" id="videoPlayback'.$k.'">';
	print '<noscript>';
	print '<iframe class="ytembed" src="'.$url.'?rel=0" allowfullscreen></iframe>';
	print '</noscript>';
	print '</div>';
	print '</li>';
	$k += 1;
      }
      print '</ul>';
      ?>
      <p>
	<a href="videos/index.php" type="text/html">
          Back
	</a>
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
