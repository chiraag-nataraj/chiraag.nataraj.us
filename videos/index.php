<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>
      Videos
    </title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        Video Links
      </h1>            
      <ul class="videoList">
        <li>
          <a href="videos/rangapravesha.php" type="text/html">
            Debut Classical Tabla Concert (raMgapravEsha, ರಂಗಪ್ರವೇಶ)
          </a>
        </li>
	<li>
	  <a href="videos/vocal.php" type="text/html">
	    Vocal performances
	  </a>
        <li>
          <a href="videos/keyboard.php" type="text/html">
            Keyboard renditions of popular songs
          </a>
        </li>
	<li>
	  <a href="videos/swara_sahitya_sudhe.php" type="text/html">
	    swara sAhitya sudhe (ಸ್ವರ ಸಾಹಿತ್ಯ ಸುಧೆ) (at AKKA 2016)
	  </a>
	</li>
        <li>
          <a href="videos/tabla.php" type="text/html">
            Tabla performances
          </a>
        </li>
      </ul>
      <h2>
	What's with the weird capitalization?
      </h2>
      Indian languages, including Kannada and Hindi, happen to contain many more consonants than English and differentiate between long and short vowels. Thus when transcribing from Hindi or Kannada to English, we end up using capitalization to differentiate between different consonants with the same Romanization and between long and short vowel sounds. More information can be found in <a href="https://en.wikipedia.org/wiki/ITRANS" target="_blank">Wikipedia's ITRANS article</a>. Bottom line is, a cat did not walk over the keyboard! 😉
      <!-- <ul class="nav">
           <li>
           <a href="http://nataraj.us/videos/index.html" type="text/html">
           All videos
           </a>
           </li>
	   </ul> -->
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
