<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Scripts and Programs</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet"/>
    <?php include('../base.php') ?>
  </head>

  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>My Scripts and Programs</h1>
      <h2>Programs</h2>
      <h3>Musik Open Source Music Creator</h3>
      <p>
        Musik is an open-source text-to-music converter. It takes text such as
      </p>
      <code>
        C4 D4 E4 F4 G4 A4 B4 C^4
      </code>
      <p>
        and translates it into audible music. It supports the following:
      </p>
      <ul class="a">
        <li><span>Basic Notes (C, D, E, etc.)</span></li>
        <li><span>Octaves (^ &amp; _)</span></li>
        <li><span>Sharps and Flats (# &amp; b)</span></li>
        <li><span>Chords - Predefined:</span></li>
        <li class="nosymbol">
          <ul>
            <li><span>Major</span></li>
            <li><span>Minor</span></li>
            <li><span>Major 7th</span></li>
            <li><span>Minor 7th</span></li>
            <li><span>Diminished</span></li>
            <li><span>Sustained</span></li>
            <li><span>Augmented</span></li>
            <li><span>Augmented 7th</span></li>
            <li><span>Add9</span></li>
          </ul>
        </li>
        <li><span>Chords - user-made</span></li>
        <li><span>Instrument changes - both in melody and in chords</span></li>
        <li><span>Multiple melody notes at the same time</span></li>
        <li><span>Scale changes</span></li>
        <li><span>Tempo changes</span></li>
        <li><span>Sections - to avoid retyping the same thing over and over again</span></li>
        <li><span>Emulate holding down the sustain pedal</span></li>
        <li><span>Tie notes together</span></li>
        <li><span>Simple beat specification using MIDI channel 10</span></li>
        <li><span>Custom instrument mappings (to access sounds in expanded sound banks)</span></li>
        <li><span>Custom beat symbol mappings (to access sounds other than the ones mapped by default)</span></li>
        <li><span>Per-file option to specify the file to save the music to</span></li>
        <li><span>Allows you to split your music into multiple musik files and import them</span></li>
      </ul>
      <p>
        You can download the current version (version 3.0 released 11/08/2016) here:
      </p>
      <ul class="a">
        <li><span><a href="programs/musik-3.0-i386-windows.zip">Windows 32-bit</a></span></li>
        <li><span><a href="programs/musik-3.0-x86_64-windows.zip">Windows 64-bit</a></span></li>
        <!-- <li><span><a href="programs/musik-2.9-x86_64-osx.zip">OSX 64-bit</a></span></li> -->
        <li><span><a href="programs/musik-3.0-i386-linux.zip">Linux 32-bit</a></span></li>
        <li><span><a href="programs/musik-3.0-x86_64-linux.zip">Linux 64-bit</a></span></li>
	<li><span><a href="programs/musik-3.0-android.apk">Android</a></span></li>
      </ul>
      <p>
	Note: Due to the fact that I am too poor to afford a Mac and my Hackintosh virtual machine broke, I will no longer be compiling for macOS. However, it should be relatively painless to compile from source on macOS with the instructions in the repository.
      </p>
      <p>
        Musik is released under the GPL version 3.0 (or later).
        <a href="https://gitlab.com/chiraag-nataraj/musik" target="_blank">
          Go to GitLab.
        </a>
      </p>
      <h3>bkmk</h3>
      <p>
	bkmk is a bookmark manager heavily inspired by <a href="https://passwordstore.org" target="_blank">pass</a>. You can find more information at the <a href="https://gitlab.com/chiraag-nataraj/bkmk">GitLab</a> repository.
      </p>
      <h2>Scripts</h2>
      
      Since I have started using Linux, I have developed a couple of scripts to make my life easier.

      <h3>iw enhanced</h3>
      <p>
	This shell script + Haskell program can be used to find a specific SSID (useful if you use <kbd>iw</kbd> to figure out the authentication parameters for a certain SSID, since the associated parameter in <kbd>iw</kbd> appears to be <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=894316" target="_blank">broken</a>).
      </p>
      <label for="findwifi">The shell script wrapper is pretty simple:</label>
      <textarea id="findwifi" class="code" readonly="readonly">
#!/bin/bash

# The following will try to automatically detect your wireless interface.
# Set it manually if this is causing problems.

IFACE=`tail -n 1 /proc/net/wireless | awk '{print $1;};' | sed 's/://g'`

if [ "$1" = "--list" ]
then
    sudo iw dev $IFACE scan | grep "SSID" | sort | uniq
else
    sudo iw dev $IFACE scan | parse_wifi.hs - $*
fi
      </textarea>
      <label for="parsewifi">The Haskell program is a bit more involved:</label>
      <textarea id="parsewifi" class="code" readonly="readonly">
#!/usr/bin/env runhaskell

{-
  Copyright 2018 Chiraag M. Nataraj

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

module ParseWifi where

import qualified Data.Text as T
import System.Environment

parse :: [String] -> [[String]]
parse x =
  let parseRec :: [String] -> [[String]] -> [[String]]
      parseRec [] y = y
      parseRec (x:xs) [] = parseRec xs [[x]]
      parseRec (x:xs) yl@(y:ys) | startsWith x "BSS" = parseRec xs ([x] : yl)
                                | otherwise = parseRec xs ((y ++ [x]) : ys)
  in
    parseRec x []

startsWith :: String -> String -> Bool
startsWith [] [] = True
startsWith [] _ = False
startsWith _ [] = True
startsWith (x:xs) (y:ys) = (x == y) && startsWith xs ys

findSSID :: String -> [[String]] -> [String]
findSSID ssid [] = []
findSSID ssid (x:xs) | any (flip startsWith ("SSID: " ++ ssid) . T.unpack . T.strip . T.pack) x = x
                     | otherwise = findSSID ssid xs

main :: IO ()
main =
  do
    args <- getArgs
    let f = head args
        ssids = tail args
    fi <-
      case f of
        "-" -> getContents
        _ -> readFile f
    let pfi = (parse . lines) fi
        found = map (`findSSID` pfi) ssids
    mapM_ (putStr . unlines) found
      </textarea>
      <p>
	Save the shell script as whatever you want (and mark it executable), and save the Haskell program as <kbd>parse_wifi.hs</kbd> somewhere in your path (and mark it executable). Calling this is pretty simple — simply type the name of the script followed by the SSIDs you wish to filter for or type <kbd>--list</kbd> to list all of the available SSIDs. Side note: Yes, they say we shouldn't screenscrape <kbd>iw</kbd>. No, I don't care. I will keep this script up to date if the format of the output changes 😉
      </p> 
      <h3>Appify</h3>
      <p>
        This shell script can be used to create a shortcut to any website using a .desktop file. It takes two inputs - the website and the name of the menu item - and creates a .desktop file which launches Firefox with the specified website. This means it automatically appears in the menu in the Gnome, KDE, and XFCE desktop environments. <a href="programs/appify.php">The appify script can be found here</a>.
      </p>
      <h3>EZmail</h3>
      <p>
        Ah - the wonders of mailing lists. I started a <a href="mate/index.php">club</a> at school and needed to email all of the members at once, preferably without any interaction from me. At       first, I resorted to emailing each member individually, but I knew that could not stand for long. Out of necessity, I was forced to come up with a script which would save the day. I use a command-line email client called <code>mutt</code>, which is capable of accepting an address, a subject, and a body. It will then automatically email the person with the subject and body I specify. I then came up with a script which passes each address in the addresses argument to mutt with the subject and body arguments also specified, optionally with an attachment also specified.
      </p>
      <label for="ezmail">The mass email script is given below:</label>
      <textarea id="ezmail" class="code" readonly="readonly">
#!/bin/bash

# Copyright 2013 Chiraag M. Nataraj
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ $# -eq 3 ]
then
echo "Sending emails"
cat "$1" | while read line; do
echo "Sending email to $line"
mutt -F ~/.muttrc.home -s "`cat $2`" $line < "$3"
done
fi
                                                 
if [ $# -eq 4 ]
then
echo "Sending emails"
cat "$1" | while read line; do
echo "Sending email to $line"
mutt -F ~/.muttrc.home -s "`cat $2`" -a "$4" -- $line < "$3"
done
fi
      </textarea>
      <p>
        All of the arguments, with the exception of the attachment, are plain text documents. The addresses file is set up with one address per line. The subject and body files are just plain text files. This script was inspired by the Windows-only program febootimail and serves my purposes well.
      </p>
      <!-- <h3>NitroRoto - Rotating Backgrounds for Linux</h3>
	   <p>
           I currently use Debian Linux and use a program called Nitrogen to set the wallpaper. I wanted rotating wallpapers, but Nitrogen does not support that. I looked around and found some scripts which will randomly select a wallpaper from all of the images in a given directory. However, that did not work for me because I have my images sorted into four folders. Therefore, I decided to try modifying the script I found. However, once I started trying to modify it, I figured out I had to pretty much write it from scratch. The script I came up with is shown below:
	   </p>
	   <textarea title="NitroRoto" class="code" readonly="readonly" rows="10" cols="80">
	   #!/bin/bash

	   # Copyright 2013 Chiraag M. Nataraj
	   # This program is free software: you can redistribute it and/or modify
	   # it under the terms of the GNU General Public License as published by
	   # the Free Software Foundation, either version 3 of the License, or
	   # (at your option) any later version.
	   #
	   # This program is distributed in the hope that it will be useful,
	   # but WITHOUT ANY WARRANTY; without even the implied warranty of
	   # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	   # GNU General Public License for more details.
	   #
	   # You should have received a copy of the GNU General Public License
	   # along with this program.  If not, see <http://www.gnu.org/licenses/>.

	   ## Set the WALLPAPERS variable to the root folder of your images (like the Pictures folder for instance). This script will automatically look in all subfolders.

	   WALLPAPERS="/home/chiraag/Pictures/backgrounds/rotate"

	   ## Reformats the path for use with sed

	   WALLMOD=$(echo $WALLPAPERS | sed -e 's/\//\\\//g')

	   ## Finds all jpg and png files in the current folder and subfolders. You can add more file types by copying one of the statements below and editing the part after -name.

	   find $WALLPAPERS -type f -name "*.jpg" | sed -e "s/$WALLMOD//" | sed -e 's/ /\\ /g'> $WALLPAPERS/files.txt
	   find $WALLPAPERS -type f -name "*.png" | sed -e "s/$WALLMOD//" | sed -e 's/ /\\ /g'>> $WALLPAPERS/files.txt

	   ## ----------------------------------------------------------------- ##

	   ## You should not need to modify anything after this point

	   RANGE=`wc -l $WALLPAPERS/files.txt | sed -e "s/$WALLMOD\/files.txt//"`
	   let "range = $RANGE"
	   let "number = $RANDOM"
	   let LASTNUM="`cat $WALLPAPERS/.last` + $number"
	   let "number = $LASTNUM % $RANGE"
	   echo $number > $WALLPAPERS/.last
	   FILE=$(sed -n "$number p" $WALLPAPERS/files.txt)
	   nitrogen --set-zoom-fill --save $WALLPAPERS/$FILE
	   </textarea>
	   <p>
           The only things similar to the original script are the variable names.
	   </p>
	   <h3>QuickRename</h3>
	   <p>
           This simple script looks for files matching a certain extension pattern and renames it to the new pattern.
	   </p>
	   <textarea title="QuickRename" class="code" rows="6" cols="80" readonly="readonly">
	   #!/bin/bash

	   # Copyright 2013 Chiraag M. Nataraj
	   # This program is free software: you can redistribute it and/or modify
	   # it under the terms of the GNU General Public License as published by
	   # the Free Software Foundation, either version 3 of the License, or
	   # (at your option) any later version.
	   #
	   # This program is distributed in the hope that it will be useful,
	   # but WITHOUT ANY WARRANTY; without even the implied warranty of
	   # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	   # GNU General Public License for more details.
	   #
	   # You should have received a copy of the GNU General Public License
	   # along with this program.  If not, see <http://www.gnu.org/licenses/>.

	   find -name *.$1 | while read f
	   do
	   mv ./"$f" "${f%$1}$2"
	   done
	   echo "Successful!"
	   </textarea>
	   <p>
           Both the extension to replace and the extension to replace it with are passed as arguments to the script. Technically, this script could be used to replace any sequence of characters at the end of files/folders - that "sequence of characters" just happens to be the extension most of the time.
	   </p> -->
      <h3>SmartPlaylist</h3>
      <p>
        This simple script simply adds smart playlist functionality to MPD/MPC (my music player of choice).
      </p>
      <label for="smartplaylist">The SmartPlaylist script itself is pretty simple:</label>
      <textarea id="smartplaylist" class="code" rows="6" cols="80" readonly="readonly">
#!/bin/bash

# Copyright 2013 Chiraag M. Nataraj
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

mpc clear;
while read l; do
    CAT=`echo $l | awk -F: '{ print $1 }'`
    VAL=`echo $l | awk -F: '{ print $2 }' | sed 's/^[[:space:]]*//g'`
    mpc search $CAT "$VAL" | mpc add
done < $1
      </textarea>
      <p>
        It reads in the file (the only argument to the script), parses it (I will explain the syntax in a minute), and replaces the current MPD playlist with the playlist generated by the script (I believe that is how most smart playlists work - if you only want to ever add songs, comment out the line <code>mpc clear;</code> and SmartPlaylist won't clear the current playlist). Now what does a typical SmartPlaylist file look like?
      </p>
      <label for="smarttext">Here's an example of a smart playlist:</label>
      <textarea id="smarttext" class="code" rows="6" cols="80" readonly="readonly">
artist: mukesh
artist: lata
artist: kalinga
artist: balasubramaniam
artist: joshi
artist: manna dey
artist: kishore
      </textarea>
      <p>
        The first value is the category you are searching and the second value is the value you are searching for within that category. You can use any search category that is a valid search category for MPC (the MPD client this script uses).
      </p>
      <h3>SimpleBackup</h3>
      <p>
        Any good computer user knows that they should back up their documents and other files. I knew I should be backing up my stuff, but I hadn't found any good, simple, backup programs for Linux. Hence, I created my own shell script that does the job for me.
      </p>
      <label for="simplebackup">My backup script:</label>
      <textarea id="simplebackup" class="code" rows="6" cols="80" readonly="readonly">
#!/bin/bash

# Copyright 2019 Chiraag M. Nataraj
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Set the environment variable below to the root of the place you want to store your backup
BACKUP_ROOT="/media/disk_by-label_Backup"
SOURCE_ROOT="/home/chiraag"
HDS=( "/dev/nvme0n1p2" "/dev/sda1" )
HEADER_BACKUP="header_backup.bin"
VDEF="current"
rsync_args=( -avuzXHEAP --delete )
LVSIZE="10G"
SNAPNAME="RootSnapshot"
SNAPSOURCE="/dev/Crypto/Root"
SNAPFILE="snapshot.img"

if [ ! -z "$1" ]
then
    BACKUP_ROOT="$1"
fi

rsync_vargs=( --copy-unsafe-links "--link-dest=$BACKUP_ROOT/$VDEF/" )

# Preparation

dpkg --get-selections > "${BACKUP_ROOT}"/installed_software

# Create a backup of the LUKS headers

for i in "${HDS[@]}";
do
    hdbackup="${BACKUP_ROOT}/${HEADER_BACKUP}.$(basename "${i}")"
    if [ -e "$hdbackup" ]
    then
       mv "$hdbackup" "${hdbackup}.bak"
    fi
    if (sudo cryptsetup luksHeaderBackup "$i" --header-backup-file "$hdbackup") && [[ -e "${hdbackup}.bak" ]]
    then
	rm "${hdbackup}.bak"
    fi
done

# Relative paths from $SOURCE_ROOT to back up
# These files and folders will not be backed up with versions

files=( ಸಂಗೀತ ಚಿತ್ರಗಳು ವಿಡಿಯೊಗಳು .config .local )

sys_files=( /etc/apt/sources.list /etc/apt/sources.list.d /etc/apt/apt.conf.d )

# These folders will be backed up with versions

vfiles=( ದಸ್ತಾವೇಜುಗಳು )

VDATE=$(date "+%Y_%m_%d")

mkdir -p "$BACKUP_ROOT"/"$VDATE"

if rsync "${rsync_args[@]}" "${rsync_vargs[@]}" "${vfiles[@]/#/$SOURCE_ROOT/}" "$BACKUP_ROOT"/"$VDATE"
then
    rm "$BACKUP_ROOT"/"$VDEF"
    ln -s "$BACKUP_ROOT"/"$VDATE" "$BACKUP_ROOT"/"$VDEF"
fi

rsync "${rsync_args[@]}" "${files[@]/#/$SOURCE_ROOT/}" "$BACKUP_ROOT"

sudo rsync "${rsync_args[@]}" "${sys_files[@]}" "$BACKUP_ROOT"

read -r -p "Backup snapshot? (y/n): " SNAP
case $SNAP in
    y|Y)
	sudo lvcreate -L"$LVSIZE" -s -n "$SNAPNAME" "$SNAPSOURCE"
	SNAPGRP=$(sudo lvs | grep "$SNAPNAME" | awk '{print $2;}')
	sudo dcfldd if=/dev/"$SNAPGRP"/"$SNAPNAME" of="$BACKUP_ROOT"/"$SNAPFILE" bs=4M
	notify-send "Finished snapshot"
	sudo lvremove /dev/"$SNAPGRP"/"$SNAPNAME"
	;;
    n|N)
	echo "Not making snapshot"
	;;
esac
      </textarea>
      <p>
        This script will efficiently backup your data (provided you configure it correctly). The only things you should have to change are BACKUP_ROOT (change to point to the directory you want to back up to - everything will end up inside this directory), SOURCE_ROOT (change to point to the directory that everything you want to back up is in - usually, the home folder will do), files (change to the list of files and folders you want to back up), vfiles (change to the list of files and folders you want to back up with versions), and possibly rsync_args (the arguments you would like to pass to rsync - the defaults I have provided should be sufficient). It also optionally makes a backup of your system partition using LVM snapshots and backups of your LUKS headers. In addition, this script saves a couple of key files like the list of sources and the list of installed software. However, this is particular to Debian and Debian-based distributions, so you may need to modify it for your distribution.
      </p>
      <h3>sCard Converter</h3>
      <p>
        The sCard format is a simple and logical way to store your contacts. A sample entry would look something like this:
      </p>
      <code>
First Name : Last Name : Phone1 \n Phone2 \n Phone3 : Email1 \n Email2
      </code>
      <p>
        However, the columns aren't predefined. That is, another sample entry could look something like this:
      </p>
      <code>
Last Name : First Name : Email1 \n Email2 : Phone1 \n Phone2
      </code>
      <p>
        This renders the format much more flexible than other formats for two reasons:
      </p>
      <ul class="a">
        <li><span>You can define your own columns and the program doesn't care.</span></li>
        <li><span>You can have as many email addresses or phone numbers or whatever for each contact and the program doesn't care.</span></li>
      </ul>
      <p>
        However, there are a couple of problems with the format. It is not accepted by any major email clients or phones, and it doesn't look that pretty. Enter the sCard Converter. This simple Python script converts from the sCard format to LaTeX (to make it look pretty) and vCard (to allow you to import your contacts into phones and email clients). The one catch is that if one is exporting to vCard, one must specify the headers at the top of the file. The header must use vCard compatible header names (<a href="https://en.wikipedia.org/wiki/Vcard#Properties" target="_blank">full list here</a>) followed by square brackets denoting how many columns the header should combine. For example, if the entry is:
      </p>
      <code>
First Name : Last Name : Phone1 \n Phone2 \n Phone3 : Email1 \n Email2
      </code>
      <p>
        then the corresponding header would be:
      </p>
      <code>
FN[2] : TEL[1] : EMAIL[1]
      </code>
      <p>
        As mentioned earlier, the header row must be the <strong>first</strong> row in the sCard file. You might as well always write a header row because the LaTeX export ignores it anyway. The LaTeX export will output a LaTeX table which you can then <code>\include</code> into a full LaTeX file to get a pretty address book!  The vCard export will output a vCard file that should be compatible with most (newer) phones and almost all webmail/email clients. The script also lets you choose which column to sort by. <a href="programs/address_book.php">The sCard converter script can be found here</a>.
      </p>
      <h3>SimpleTag</h3>
      <p>
        <a href="programs/tag.php">This simple Python script</a> uses extended attributes (supported by all common filesystems (although OS X programs apparently delete them)) to store tags for files and folders. The script allows adding tags, deleting all tags for a given file, removing specific tags for a given file, listing currently used tags, and searching for files with a specific tag. It requires the <a href="http://github.com/xattr/xattr" target="_blank">xattr</a> module. That module apparently only supports Linux, Mac OS X, FreeBSD, and Solaris at present, although I have successfully tagged files on an NTFS file system.
      </p>
      <p>
        I have developed other scripts, but they are not as useful as these and are mostly fixes for certain issues I was having at the time.
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
