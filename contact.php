<!DOCTYPE html5>
<?php include('hsts.php') ?>
<html lang=en>
  <head>
    <title>Contact Chiraag</title>
    <link href="main.css" type="text/css" rel="stylesheet">
    <?php include('base.php') ?>
  </head>
  <body>
    <?php include('navigation.php') ?>
    <div id="content">
      <h1>
	Request Source
      </h1>
      <p>
        All fields are required.
      </p>
      <form name="sourcerequest" method="post" action="source_request.php">
        First Name<sup>*</sup>: <input type="text" name="firstname">
        <br />
        Last Name<sup>*</sup>: <input type="text" name="lastname">
        <br />
        Email address<sup>*</sup>: <input type="text" name="email">
        <br />
        Reason<sup>*</sup>:
        <br />
        <textarea required="required" name="reason" rows="10" cols="80"></textarea>
        <br />
        <input type="submit" value="Submit" />
      </form>
      <hr width="100%" align=center>
      <?php include('footer.html') ?>
    </div>
  </body>
</html>
