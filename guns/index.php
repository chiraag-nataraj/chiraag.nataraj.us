<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Gun Deaths and What To Do About Them</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
	Gun Deaths and What to Do About Them
      </h1>
      <p>
	Let's talk gun deaths. Obviously mass shootings are the most visible example of this, but as horrible as they are, they make a up a tiny percentage of the total gun deaths per year.
      </p>
      <p>
	Looking at the official statistics from 2014 (the most recent data as far as I'm aware), we find that 33,594 people were killed by firearms (page 87 from <a href="https://www.cdc.gov/nchs/data/nvsr/nvsr65/nvsr65_04.pdf" target="_blank" type="applications/pdf">https://www.cdc.gov/nchs/data/nvsr/nvsr65/nvsr65_04.pdf</a>). Of these, 21,386 (or 64%) were suicides, and 11,008 (or 33%) were homicides. There are other categories, but these are the main categories which make up 97% of all firearm-related deaths.
      </p>
      <p>
	Now let us take a look at all firearm-related deaths in other developed countries. Looking at <a href="https://en.wikipedia.org/wiki/List_of_countries_by_firearm-related_death_rate" target="_blank" type="text/html">https://en.wikipedia.org/wiki/List_of_countries_by_firearm-related_death_rate</a>, it seems that the USA tops the list among developed countries at 11th place globally. The next developed country is Finland, which registers at 22nd place. And you can go down the list, but it's fairly clear that the USA is an outlier in this area. So naturally, the question is: why? But before we do that, let's take a look at variations between states.
      </p>
      <p>
	Looking at <a href="https://en.wikipedia.org/wiki/Firearm_death_rates_in_the_United_States_by_state" target="_blank" type="text/html">https://en.wikipedia.org/wiki/Firearm_death_rates_in_the_United_States_by_state</a>, we see that Alaska, Louisiana, Alabama, Mississippi, and Wyoming topping the list while Hawaii, Massachusetts, New York, Connecticut, and Rhode Island come last. We'll come back to this, but the order is interesting, more so than either main side in this debate makes it seem (for example, Texas is pretty low <em>and</em> has pretty lax gun laws).
      </p>
      <p>
	So let us now come back to the question of <em>why</em> we have such a huge firearm death rate. To do so, it may be instructive to look at Table 14 (page 67) of the CDC data. If we look at the suicide rate broken down by race and sex, we find interesting things:
      </p>
      <ol class="a">
	<li>
	  <span>
	    Men are <strong>far</strong> more likely to commit suicide with firearms in aggregate (11.7 vs 1.9).
	  </span>
	</li>
	<li>
	  <span>
	    White men are <strong>far</strong> more likely to commit suicide with firearms than any other racial or sexual group (13.7).
	  </span>
	</li>
	<li>
	  <span>
	    The next highest group is Native American or Alaskan Native men (7.1).
	  </span>
	</li>
      </ol>
      <p>
	I should note here that the numbers are a little more equal among different racial groups when it comes to suicide by other means, but white men still come out ahead.
      </p>
      <p>
	If we go ahead and look at the homicide rate broken down by race and sex, we again find interesting things:
      </p>
      <ol class="a">
	<li>
	  <span>
	    Men are still more likely to be a victim of homicide by firearm in aggregate, but the disparity is less than with the suicide statistics.
	  </span>
	</li>
	<li>
	  <span>
	    Black men are <strong>far</strong> more likely to be a victim of homicide by firearm than any other racial or sexual group (26.9).
	  </span>
	</li>
	<li>
	  <span>
	    The next highest group is Native American or Alaskan Native men (4.5).
	  </span>
	</li>
      </ol>
      <p>
	As before, the numbers are more equal among different racial groups when it comes to homicide by other means, but black men still come out ahead.
      </p>
      <p>
	So what does this data tell us? It tells us that killings of black men are driving up the homicide rate and white men are driving up the suicide rate. The question is what to do about this.
      </p>
      <p>
	Will banning or reducing access to guns reduce this carnage? Maybe. One thing that is almost certainly true is that forcing people considering suicide to use a method other than a gun will lead to more people who fail on the first try (<a href="https://www.hsph.harvard.edu/means-matter/means-matter/case-fatality/" target="_blank" type="text/html">https://www.hsph.harvard.edu/means-matter/means-matter/case-fatality/</a>), which <strong>will</strong> reduce the number of people who commit suicide. So while it is true that people who are considering suicide may just move to other methods, those other methods are provably less fatal, which will lead to fewer people dying as a result.
      </p>
      <p>
	If we look at the general statistics again (page 87 here: <a href="https://www.cdc.gov/nchs/data/nvsr/nvsr65/nvsr65_04.pdf" target="_blank" type="applications/pdf">https://www.cdc.gov/nchs/data/nvsr/nvsr65/nvsr65_04.pdf</a>), we find that a total of 15,872 people were killed by others (homicide). Of these, 11,008 (or 69%) were killed by firearm. The next known cause of death is cut/pierce. Note, though, that there are a large number of homicides (1,585) where the cause of death is unspecified, and there are 4,597 deaths where the type of death is undetermined (it could very well be that some of those are homicides, which would change the numbers somewhat). Nevertheless, let us proceed with the cases where we know the type and cause of death.
      </p>
      <p>
	Let's look at the breakdown in terms of relationship between killer and victim in cases of homicide (<a href="https://ucr.fbi.gov/crime-in-the-u.s/2014/crime-in-the-u.s.-2014/tables/expanded-homicide-data/expanded_homicide_data_table_10_murder_circumstances_by_relationship_2014.xls" target="_blank" type="text/html">https://ucr.fbi.gov/crime-in-the-u.s/2014/crime-in-the-u.s.-2014/tables/expanded-homicide-data/expanded_homicide_data_table_10_murder_circumstances_by_relationship_2014.xls</a>). Some interesting notes here:
      </p>
      <ol>
	<li>
	  The single biggest (known) circumstance category in terms of total murder victims is "Other arguments" at 2,786. Within this, the biggest relationship is Acquaintance.
	</li>
	<li>
	  "Gangland killings" (defined as a murder carried out by organized criminals (<a href="https://en.wikipedia.org/wiki/Gangland_killing" target="_blank" type="text/html">https://en.wikipedia.org/wiki/Gangland_killing</a>)), adult and juvenile, total 715 - 6% of all murders as tracked by the FBI.
	</li>
      </ol>
      <p>
	So these are interesting, right. Because the biggest (known) circumstance surrounding homicide is a type of argument between acquaintances (which apparently includes homosexual couples - this is from 2014). Further, organized criminals account for a relatively small percentage of total murders. Note that this also doesn't include drug traffickers (the category labeled "Narcotic drug laws") which comes under "Felony type" rather than "Other than felony type". This would suggest that killings between gangs (or by gangs) account for a relatively small proportion of the total murders. Just a note that the circumstances of the murder are unknown for 37.7% of the murders tracked here, and I'm not sure how those would spread out - for example, if a majority of those happened to fit in one main category or sub-category, that would skew the numbers with respect to circumstance. In the same vein, the relationship of killer with victim is unknown for 45.5% of murders, so it's also unclear if the statistics are skewed by that. I am also unclear as to why the FBI recorded a different number of homicides (11,961) than the CDC (15,872) - maybe they are using different definitions?
      </p>
      <p>
	Now let us dive into statistics based on race and sex (page 67 here: <a href="https://www.cdc.gov/nchs/data/nvsr/nvsr65/nvsr65_04.pdf" target="_blank" type="applications/pdf">https://www.cdc.gov/nchs/data/nvsr/nvsr65/nvsr65_04.pdf</a>). Some things stand out:
      </p>
      <ol>
	<li>
	  Black males are the biggest risk group for dying by homicide, with Native American males being the next biggest risk group.
	</li>
	<li>
	  However, from above, it's unclear whether most of this is due to gang activity (which is what some people claim).
	</li>
      </ol>
      <p>
	So we know that the tool used most often in homicide is a gun, black males are most likely to be killed, and gang violence most likely accounts for a small percentage of total homicides. So now the question is: what do we do?
      </p>
      <p>
	Honestly, I am not sure. If many of these murders are spontaneous (which seems plausible given the distribution of circumstances), it seems reasonable that restricting gun ownership wouldn't really do much. Why? Because in the heat of the moment, they will pick up some other weapon or improvise one out of other stuff around them - this wasn't a planned murder and their objective is to hurt/kill the other person (for whatever reason). And while there <em>may</em> be a slight uptick in survival after the attack if a gun is not used (for example, if someone tries to use a knife and is just really bad at it or stabs in the wrong place, or if they aren't strong enough to hit someone over the head with some blunt object), I'm not sure how much that effect would be - it's certainly something to look into. That is, <em>if</em> the survival after attacks would go up an appreciable amount (as in the case of suicide), there <strong>is</strong> a case to be made that banning all guns would help decrease (successful) homicides. However, that is a big <em>if</em> that needs to be studied in detail.
      </p>
      <p>
	Looking at a couple of different datasets here (<a href="https://webappa.cdc.gov/sasweb/ncipc/mortrate.html" target="_blank" type="text/html">https://webappa.cdc.gov/sasweb/ncipc/mortrate.html</a> and <a href="https://webappa.cdc.gov/sasweb/ncipc/nfirates.html" target="_blank" type="text/html">https://webappa.cdc.gov/sasweb/ncipc/nfirates.html</a>), we get an interesting picture (I am looking at the Homicide rates in the first one and Assault rates in the second one):
      </p>
      <ol class="a">
	<li>
	  <span>
	    1,740/131,090 = 1.3% chance of death by cut/pierce
	  </span>
	</li>
	<li>
	  <span>
	    56/10,496 = 0.5% chance of death by transportation (all)
	  </span>
	</li>
	<li>
	  <span>
	    11,008/71,478 = <strong>15.4%</strong> chance of death by gun
	  </span>
	</li>
	<li>
	  <span>
	    89/6,433 = 1.4% chance of death by fire
	  </span>
	</li>
      </ol>
      <p>
	In each of these cases, I looked at deaths / (deaths + nonfatal injuries), which should give a rough estimate of rate of death from each of these tools. Of course, one assumption I am making here is that the cases of assault were meant to be fatal and failed, which is a large assumption. If this is the case, though, it seems that guns <strong>are</strong> much more fatal than than other methods of assault and murder. That is, it could very well be that banning all civilian gun ownership would help bring down successful homicide rates.
      </p>
      <p>
	Some people will argue that any restriction on guns is an infringement on the Second Amendment. So let's take a look at that. All of my information is coming from <a href="https://en.wikipedia.org/wiki/Second_Amendment_to_the_United_States_Constitution" target="_blank" type="text/html">https://en.wikipedia.org/wiki/Second_Amendment_to_the_United_States_Constitution</a>. The SCOTUS had repeatedly ruled (US v. Cruikshank, US v. Miller) that the Second Amendment does not guarantee the right to bear arms. However, that changed in 2008 (District of Columbia v. Heller), when they ruled that the Second Amendment does, indeed, grant individuals the right to bear arms. Taking a look at the exact text of the version ratified by Jefferson, we see the following: "A well regulated militia being necessary to the security of a free state, the right of the people to keep and bear arms shall not be infringed." It seems that the second part of that (the right to keep and bear arms) depends on the first (a well regulated militia). That is, the right to keep and bear arms seems to be dependent (at least from my reading of the Second Amendment) on serving in a militia, specifically one that defends the country. Given that now we have a standing army and the most powerful military in the world, do we really need militias? Yeah, we needed them when we distrusted a standing army (back when the country was first formed). But at this point, it seems that that way of thinking, and that mode of defense, has become largely obsolete. Further, the "well regulated" part of the "well regulated militia" seems to always be lost in these debates. That being said, maybe I'm reading the Second Amendment incorrectly - that is entirely possible as I am not a Constitutional scholar!
      </p>
      <p>
	All of this being said, we need to be having this debate at the very least. We need to be discussing what we can do to prevent this carnage, whether in the form of needless suicides, mass shootings, or other homicides. I outlined above a couple of reasons why I think banning or restricting guns would help with that.
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
