<div class="navigation">
  <nav id="navigation">
    <ul class="nav">
      <li>
	<label for="menu" class="mobile_menu">
	  &#9776;
	</label>
	<input type="checkbox" id="menu" />
	<ul class="nav">
	  <li class="space">
	  </li>
	  <li>
            <a href="index.php" type="text/html">
              Home &#9656;
            </a>
	  </li>
	  <li class="space">
	  </li>
	  <li>
	    <label for="research" class="navtree_label">
	      Research &#9662;
	    </label>
	    <input type="checkbox" id="research" />
	    <ul class="nav">
	      <li>
		<a href="reports/index.php" type="text/html">
		  Overview
		</a>
	      </li>
	      <li>
		<a href="reports/undergrad-thesis.pdf" target="_blank" type="applications/pdf">
		  Undergraduate Thesis
		</a>
	      </li>
	      <li>
		<a href="reports/kinect-report.pdf" target="_blank" type="applications/pdf">
		  Optimizing Spinal Cord Injury Therapy
		</a>
	      </li>
	      <li>
		<a href="reports/hydraulics-report.pdf" target="_blank" type="applications/pdf">
		  Hydraulics for Micro-Robotic Actuation
		</a>
	      </li>
	      <li>
		<a href="reports/nims-report.pdf" target="_blank" type="applications/pdf">
		  Modeling of NIMs for &#8220;HIPS&#8221;
		</a>
	      </li>
	      <li>
		<a href="reports/chaos-report.pdf" target="_blank" type="applications/pdf">
		  Complexity in Nonlinear Dynamic Systems
		</a>
	      </li>
	      <li>
		<a href="reports/swarm-report.pdf" target="_blank" type="text/html">
		  Swarm Robotics
		</a>
	      </li>
	    </ul>
	  </li>
	  <li class="space">
	  </li>
	  <li>
	    <label for="essays" class="navtree_label">
	      Essays &#9662;
	    </label>
	    <input type="checkbox" id="essays" />
	    <ul class="nav">
	      <li>
		<a href="guns/index.php" type="text/html">
		  Gun Deaths and What to Do About Them
		</a>
	      </li>
	      <li>
		<a href="bullying/index.php" type="text/html">
		  Bullying - the Invisible Problem
		</a>
	      </li>
	      <li>
		<a href="usvsussr/index.php" type="text/html">
		  US vs. USSR
		</a>
	      </li>
	    </ul>
	  </li>
	  <li class="space">
	  </li>
	  <li>
            <a href="blog/index.php" type="text/html" target="_blank">
              Blog &#9656;
            </a>
	  </li>
	  <li class="space">
	  </li>
	  <li>
	    <label for="government" class="navtree_label">
	      Government &#9662;
	    </label>
	    <input type="checkbox" id="government" />
	    <ul class="nav">
	      <!-- <li>
		   <a href="government/executive_orders/index.php" type="text/html">
		   Executive Orders &amp; Memoranda
		   </a>
		   </li> -->
	      <li>
		<a href="government/hearings/index.php" type="text/html">
		  Hearings
		</a>
	      </li>
	      <li>
		<a href="government/nominations/index.php" type="text/html">
		  Nomination Tracker
		</a>
	      </li>
	    </ul>
	  </li>
	  <li class="space">
	  </li>
	  <li>
	    <label for="media" class="navtree_label">
	      Media &#9662;
	    </label>
	    <input type="checkbox" id="media" />
	    <ul class="nav">
	      <li>
		<a href="photos/index.php" type="text/html">
		  Photos
		</a>
	      </li>
	      <li>
		<a href="videos/index.php" type="text/html">
		  Videos
		</a>
	      </li>
	    </ul>
	  </li>
	  <li class="space">
	  </li>
	  <li>
	    <label for="coding" class="navtree_label">
	      Coding &#9662;
	    </label>
	    <input type="checkbox" id="coding" />
	    <ul class="nav">
	      <li>
		<a href="programs/index.php" type="text/html">
		  Some Scripts &amp; Programs
		</a>
	      </li>
	      <li>
		<a href="latex/index.php" type="text/html">
		  An Introduction to LaTeX
		</a>
	      </li>
	      <li>
		<a href="firefox/index.php" type="text/html">
		  Firefox Tips and Tricks
		</a>
	      </li>
	      <li>
		<a href="wireless/index.php" type="text/html">
		  Minimalist Wireless Networking
		</a>
	      </li>
	      <li>
		<a href="passwords/index.php" type="text/html">
		  2FA Password Manager
		</a>
	      </li>
	    </ul>
	  </li>
	  <li class="space">
	  </li>
	  <li>
            <a href="privacy.php">
              Privacy Policy &#9656;
            </a>
	  </li>
	  <li class="space">
	  </li>
	  <li>
	    <label for="contact" class="navtree_label">
	      Contact me &#9662;
	    </label>
	    <input type="checkbox" id="contact" />
	    <ul class="nav">
	      <li>
		<a href="feedback/index.php" type="text/html">
		  Feedback
		</a>
	      </li>
	      <li>
		<a href="mailto:chiraag@chiraagnataraj.us">
		  Email
		</a>
	      </li>
	    </ul>
	  </li>
	</ul>
      </li>
    </ul>
  </nav>
</div>
