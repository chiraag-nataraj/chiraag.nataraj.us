<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Facebook Rants</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        Facebook Rants
      </h1>
      <p>
	This is a list of my Facebook rants listed in chronological order.
      </p>
      <ul class="a">
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10154334530604909" target="_blank">Free trade and colonialism</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10154655633039909" target="_blank">What a Trump presidency might mean</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155053387119909" target="_blank">Discussion of the AHCA</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155376008254909" target="_blank">Philando Castille</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155428612984909" target="_blank">Ethical consumption</a></li>
	<li><a href="https://www.facebook.com/notes/%E0%B2%9A%E0%B2%BF%E0%B2%B0%E0%B2%BE%E0%B2%97%E0%B3%8D-%E0%B2%A8%E0%B2%9F%E0%B2%B0%E0%B2%BE%E0%B2%9C%E0%B3%8D/that-google-memo-thing/10154925445443546/" target="_blank">The Google Memo</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155572036679909" target="_blank">Slavery</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155589396509909" target="_blank">Self-segregation</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155624083274909" target="_blank">Terrorism</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155640971754909" target="_blank">Hillary Clinton's interview</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155659869119909" target="_blank">Military spending</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155680777019909" target="_blank">Gun deaths Part 1: Suicide</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155689588474909" target="_blank">Gun deaths Part 2: Homicide</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155848567799909" target="_blank">War on Christians</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10155948791224909" target="_blank">Western countries and democracy</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10156101266659909" target="_blank">Reducing plastic use</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10156414419469909" target="_blank">Family separations</a></li>
	<li><a href="https://www.facebook.com/chiraag.nataraj/posts/10156747248754909" target="_blank">Why I'm voting #UnionYES!</a></li>
      </ul>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
