<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Bullying &mdash; the Invisible Problem</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
        Bullying &mdash; the Invisible Problem
      </h1>
      <p>
        The day was Halloween. The time was 2:30 PM. It was like a normal school day (although many people were dressed up in outrageous costumes), and I walked calmly towards my bus. My school has an outer ring and an inner ring for the buses, and my bus was always close to the school, although on the inside ring. I got on, and there were very few people on at the time. I sat down slightly nearer the back side of the bus, which was to have an enormous impact in just a few minutes. I was contentedly sitting in my seat when I heard a loud commotion. Immediately afterwards, two kids came running into the bus.
      </p>
      <p>
        One, Joe, was tall and very strong, muscles bulging out. I had had several experiences in the past which were less than pleasant, and I always tried to avoid him. Close on his tail was Schmoe, a close friend of mine. Schmoe had dressed up as Ciel Phantomhive (from the anime Kuroshitsuji) and had worn a bluish-blackish wig that day. While coming onto the bus, Joe had stolen Schmoe's wig and run. Schmoe followed on his tail. Joe went to the back of the bus and taunted Schmoe. "Give me back my wig!" Schmoe shouted. "Never! Haha!" replied Joe. In response, Schmoe "punched" him in the gut (it was more of a shove) and took the wig back. Joe had never been punched before, and took it really personally. He started making "jokes" about Schmoe, calling him homosexual and other, unprintable, names. I was gradually getting very, very angry. However, I managed to hold it in for the time being, as Schmoe was coping relatively well with the bullying. Schmoe kept telling Joe to stop making fun of him, but Joe did not listen. After about five or ten minutes, we arrived at Schmoe's stop, so Schmoe left. However, that is not the end of the story - indeed, all of this was just a preface, an introduction to the characters if you will.
      </p>
      <p>
        I thought Joe would stop taunting Schmoe after he (Schmoe) left the bus. However, Joe insisted on making fun of Schmoe's "punch" and how it was extremely weak. He also kept going with the homosexual theme. I could stand it no longer. I could vividly recall the taunts and insults I had suffered as a result of this one boy, and I could not let this go unchecked. "Shut up!" I said. "What? You just asked me to shut up?" he replied. I replied in the affirmative. Joe looked as furious as a matador's bull. He started making fun of me, taunting me about my mustache, my Halloween costume, and generally cursing at me. I grew furious with him and also cursed at him. Finally, just before he had to leave the bus, he said "Do you know who you're dealing with?" He is on the wrestling team, and, as I had mentioned earlier, extremely strong. I replied "I don't really care". About two weeks later, Joe himself, without any prompting from anyone, came to me and apologized for the "incident", and he hasn't taunted him or me since.
      </p>
      <p>
        This is a troubling incident.  However, I did not go into the real issue - almost <em>none</em> of Schmoe's friends stood up for him (I and this one girl stood up for him, but that was it). In fact, most of his 'friends' were <em>laughing</em> at him.  <em>Why?</em> Why didn't Schmoe's friends stick up for him?  Was it because they suspected he was gay and therefore didn't want to be associated with him?  Was it the fact that if they stood up for him, they might be bullied too?  Was it the spectator effect?
      </p>
      <p>
        Whatever it is, it has <strong>got</strong> to stop.  Bullying has, is, and always will be an issue.  But that does not mean everyone can just walk away and pretend like it's not happening.  Because the reality, to thousands of kids, is that it is.  And while we may not be able to prevent all bullying, we sure can provide a support network for bullied students.  Because no one should have to go through what Schmoe went through.  He was almost on his own that day.  Now, he was a strong kid, so he would have been fine.  But thousands of kids face bullying simply because they are different from 'the norm'.  And here we come to a huge problem: What's 'normal'?  As far as I can tell, <em>nothing</em> is normal.  Everyone is unique.  There are around 7 billion people on this Earth, and not <em>one</em> is <strong>you</strong>.  <strong>You</strong> are unique.  Similarly, the people around you are unique.  So stop acting like there is a 'normal', because there really isn't.
      </p>
      <p>
        Now let's examine bullies.  Most of the time, they are <em>not</em> hateful creatures which should be wiped off of the face of the earth (contrary to the beliefs of the bullied).  The problem is that bullies are usually themselves bullied and like to take their frustration out on other people by bullying them.  So there is no real sense in punishing a bully, because that does not address the underlying cause of his/her actions.  What schools need to do send bullies (and the bullied) to therapists, where they can get help, feel better about themselves, etc.
      </p>
      <p>
        Bullying is the Invisible Problem simply because so many people go through it, yet there is so little activism about it.  Sure, every school has laws against bullying - ours did.  But no one ever, <em>ever</em> tells on a bully - that <strong>will</strong> make the problem worse.  No, what we need is action at the student level.  <strong>Don't</strong> just stand there when you see a person being bullied.  It doesn't matter who it is.  It could be your best friend or your worst enemy.  If you see someone being bullied, <strong>SAY SOMETHING</strong>.  Go up there and say "Hey!  Stop it!".  If they turn on you, you can count on the person who was being bullied to defend you - he/she knows what it's like to feel worthless, depressed, and ready to kill oneself.  So let this be the end of inaction.  If you see bullying, intervene.  Please.  The health and safety of our peers depends on it.
      </p>
      <p>
        I was inspired by the following essays:
      </p>
      <ul class="a">
        <li><a href="http://danoah.com/2011/11/im-christian-unless-youre-gay.html" target="_blank">I'm Christian Unless You're Gay</a></li>
        <li><a href="http://www.danoah.com/2010/10/memoirs-of-bullied-kid.html" target="_blank">Memoirs of a Bullied Kid</a></li>
        <li><a href="http://www.danoah.com/2011/10/bullied-the-forgotten-memoirs.html" target="_blank">Bullied.  The Forgotten Memoirs</a></li>
      </ul>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
