\contentsline {section}{\numberline {1}Preface}{2}{section.1}
\contentsline {section}{\numberline {2}Introduction - Limits}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}What is a limit?}{3}{subsection.2.1}
\contentsline {section}{\numberline {3}Derivatives}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}The formula for the derivative}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}The exponential and logarithmic functions}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Polynomial derivatives}{5}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Product and Quotient Rules}{6}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Trigonometric Functions}{7}{subsection.3.5}
\contentsline {section}{\numberline {4}Application of Derivatives}{10}{section.4}
\contentsline {section}{\numberline {5}The Concept of the Integral}{11}{section.5}
\contentsline {section}{\numberline {6}Applications of Integrals}{14}{section.6}
\contentsline {section}{\numberline {7}Sequences and Series}{14}{section.7}
\contentsline {section}{\numberline {8}Maclaurin and Taylor polynomials/series}{14}{section.8}
\contentsline {section}{\numberline {9}Revisiting limits}{14}{section.9}
\contentsline {section}{\numberline {10}Computing Error}{14}{section.10}
