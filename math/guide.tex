\documentclass[12pt]{article}
\usepackage[top=1in,bottom=1in,left=1in,right=1in]{geometry}
\usepackage{amsmath}
\usepackage{hyperref}
\hypersetup{
colorlinks=true,
linkcolor=black
}
\setlength{\parindent}{0pt}
\begin{document}
\title{Chiraag's Guide to Calculus (I and II)}
\author{Chiraag M. Nataraj}
\maketitle
\tableofcontents
\pagebreak
\section{Preface}
To the reader:

I was motivated to write this guide to Calculus by my experience in the public school system.  I realized that the teacher would just make us memorize things without actually going through the proofs/reasons.  Now, this was not his fault at all.  The whole AP program is structured such that one is forced to teach by rote memorization.  Now, this is perfectly fine for the short term (i.e. the next test), but is an absolute failure for long term retention.  I hope this guide will help you, as the reader, understand a little more of where a lot of those rules come from.

- Chiraag Nataraj
\pagebreak
\section{Introduction - Limits}
\subsection{What is a limit?}
The limit of a function $f(x)$ as x approaches a value $c$ is the value the function $f(x)$ approaches as x approaches $c$.  This is often written as $\displaystyle\lim_{x\to c}f(x)$.  The main question is how exactly to evaluate this.  Let's take a simple function: $y=x^2$.  Let's take this limit: $\displaystyle\lim_{x\to 10}x^2$.  The first thing to try is directly plugging in $x=10$ into the function.  Does this cause a division by zero or another ``bad'' thing?  Nope!  $10^2$ is perfectly okay to do: it equals 100!  Therefore, $\displaystyle\lim_{x\to 10}x^2 = 100$.  Now let's try another, more complicated example: $\displaystyle\lim_{x\to 0} \frac{x^2}{x}$.  Hmmm...let's try just plugging in $x=0$.  Oops!  $\frac{0}{0}$ is undefined!  But let's take a closer look at the function.  $\frac{x^2}{x} = x, x \neq 0$.  But the fact that the function is not \emph{defined} at $x=0$ does not mean the function doesn't have a \emph{limit} at $x=0$.

\section{Derivatives}

\subsection{The formula for the derivative}

You've always learned that $\frac{d}{dx} f(x) = \displaystyle\lim_{\Delta x \to 0} \frac{f(x + \Delta x) - f(x)}{\Delta x}$.  But why?  Let's figure out.

What do we know?  We know that this thing, called the derivative, is the slope of the tangent line at any point $(x, f(x))$.  Now let's ignore the tangent part and just consider a line between two points on the function $(x_1,y_1)$ and $(x_2,y_2)$.  Then, the slope of the line going through both of those points would be $\frac{y_2 - y_1}{x_2 - x_1}$.  That's just basic algebra.  We can also write $x_2$ and $y_2$ as ``functions'', so to speak, of $x_1$ and $y_1$.  How?  Well, $x_2 = x_1 + \Delta x$, where $\Delta x$ represents the change in $x$ from the initial value $x_1$.  Now, let's plug in $x_1 + \Delta x$ for $x_2$ in the fraction above.  This also means that $y_2 = f(x_1 + \Delta x)$ because $y_2 = f(x_2)$.  Now, our fraction looks like this: $\frac{f(x_1 + \Delta x) - f(x_1)}{x_1 + \Delta x - x_1}$.  Okay, so we're getting a little closer.  The bottom simplifies to $\Delta x$.  Now let's go back to the tangent bit I was talking about earlier.  For a line to be a tangent line, it must hit the function at exactly one point.  This means, then, that $\Delta x \to 0$.  Of course, we can't just \emph{do} that.  Why?  Take a look at the fraction again.  We'd get $\frac{0}{0}$!  And that would be bad.  \emph{Very, very} bad.  Therefore, we write it as a limit.  Now, the fraction looks like this: $\displaystyle\lim_{\Delta x \to 0} \frac{f(x_1 + \Delta x) - f(x_1)}{\Delta x}$.  Now remember that $x_1$ was some arbitrary point.  This means we can just make it $x$ and we now have the ``normal'' form of the derivative: $\displaystyle\lim_{\Delta x \to 0}\frac{f(x + \Delta x) - f(x)}{\Delta x}$.

I'm going to follow a slightly different approach than the one I was taught.  I am going to start with the derivative of $y=e^x$.  Most of you probably know that $\frac{d}{dx}e^x = e^x$.  But \textbf{why} does it work?  Let's find out!

\subsection{The exponential and logarithmic functions}

I will start with the proof of $\frac{d}{dx} \ln{x}$.

\begin{align*}
y &= \ln{x} \\
y' &= \displaystyle\lim_{\Delta x\to 0} \frac{\ln{\left(x + \Delta x\right)} - \ln{x}}{\Delta x} \\
&= \displaystyle\lim_{\Delta x \to 0} \frac{1}{\Delta x}\ln{\frac{x + \Delta x}{x}} \\
&= \displaystyle\lim_{\Delta x \to 0} \left(\ln{\frac{x + \Delta x}{x}}\right)^{\frac{1}{\Delta x}} \\
&= \displaystyle\lim_{\Delta x \to 0} \left(\ln{\left(1 + \frac{\Delta x}{x}\right)}\right)^{\frac{1}{\Delta x}} \\
\textrm{Let }\frac{\Delta x}{x} = \frac{1}{n} \\
\textrm{Then }\displaystyle\lim_{\Delta x \to 0} = \displaystyle\lim_{n \to \infty}
\end{align*}

Think about it.  If $\Delta x \to 0$ and $n = \frac{x}{\Delta x}$, then $n \to \infty$.  Therefore:

\begin{align*}
y' &= \displaystyle\lim_{n \to \infty} \left(\ln{\left(1 + \frac{1}{n}\right)}\right)^{\frac{n}{x}} \\
&= \frac{1}{x}\ln{\displaystyle\lim_{n \to \infty} \left(1 + \frac{1}{n}\right)^n} \\
&= \frac{1}{x}\ln{e} \\
&= \frac{1}{x}
\end{align*}

Here, I used the \emph{definition} of e as $\displaystyle\lim_{n\to\infty}\left(1 + \frac{1}{n}\right)^n$ to simplify the limit.  Now that we have the derivative of $\ln{x}$, we can use it to derive the derivative of $e^x$.

\begin{align*}
y &= e^x\\
\ln{y} &= x \\
\frac{y'}{y} &= 1 \\
y' &= y \\
&= e^x \\
\end{align*}

Here, I have made use of something called the Chain Rule.  The Chain Rule states that $\frac{d}{dx} f(g(x)) = f'(g(x))g'(x)$.  However, it is a long proof and I will not show it here - it is available at \url{http://math.rice.edu/~cjd/chainrule.pdf} if you are interested.  This rule can also be used to generalize that $\frac{d}{dx} e^{u(x)} = e^{u(x)}u'(x)$.  From now on, I will only prove the case $y=f(x)$, as the Chain Rule can then be applied to generalize the results.

While we are at it, let's use an arbitrary base $b$ instead of the base $e$.

\begin{align*}
y &= b^x \\
\ln{y} &= x\ln{b} \\
\frac{y'}{y} &= \ln{b} \\
y' &= b^x\ln{b}
\end{align*}

As you can see, I made use of the properties of logs to make the derivative quite an easy one.

\begin{align*}
y &= \log_b x
\end{align*}

Now let's see if you remember the Change of Base rule, which states that $\log_b x = \frac{\log_c x}{\log_c b}$, for any base $c$.  This is \emph{extremely} useful in this case.  Let's see how:

\begin{align*}
y &= \log_b x \\
&= \frac{\ln x}{\ln{b}} \\
y' &= \frac{\frac{1}{x}}{\ln{b}} \\
&= \frac{1}{\ln{b}}\frac{1}{x}
\end{align*}

And there we go!

\subsection{Polynomial derivatives}

We now have two derivatives which don't look like much: the derivative of $e^x$ and $\ln{x}$ (and their generalized counterparts).  However, interestingly enough, we can use this to prove the ``most basic'' rule: $f'(x^n) = nx^{n-1}$.

\begin{align*}
y &= x^n \\
\ln{y} &= n\ln{x} \\
\frac{y'}{y} &= \frac{n}{x} \\
y' &= \frac{ny}{x} \\
&= \frac{nx^n}{x} \\
&= nx^{n-1}
\end{align*}

\subsection{Product and Quotient Rules}

As you can see, many of those ``rules'' are merely shortcuts for quick problems.  The main problem is that no one actually knows \emph{why} the derivative of something is what it is, which leads to confused memorization and screwing up big time on tests.  These aren't long proofs (except for the first one) and would only take a minute in class.  Now let's see why the product and quotient rules are what they are:

\begin{align*}
y &= f(x)g(x) \\
y' &= \displaystyle \lim_{\Delta x \to 0} \frac{f(x + \Delta x)g(x + \Delta x) - f(x)g(x)}{\Delta x}
\end{align*}

Now let's let $u = f(x)$ and $v = g(x)$.  Then, $f(x + \Delta x) = u + \Delta u$ and $g(x + \Delta x) = v + \Delta v$.  This is because a change $\Delta x$ changes $u$ by $\Delta u$ and changes $v$ by $\Delta v$.  Therefore:

\begin{align*}
y' &= \displaystyle \lim_{\Delta x \to 0} \frac{(u + \Delta u)(v + \Delta v) - uv}{\Delta x} \\
&= \displaystyle \lim_{\Delta x \to 0} \frac{uv + u\Delta v + v\Delta u + \Delta u \Delta v - uv}{\Delta x} \\
&= \displaystyle \lim_{\Delta x \to 0} \frac{u\Delta v + v\Delta u + \Delta u \Delta v}{\Delta x} \\
&= \displaystyle \lim_{\Delta x \to 0}\frac{u\Delta v}{\Delta x} + \lim_{\Delta x \to 0}\frac{v\Delta u}{\Delta x} + \lim_{\Delta x \to 0}\frac{\Delta u \Delta v}{\Delta x}\\
&= u\frac{dv}{dx} + v\frac{du}{dx} + \frac{du}{dx}\frac{dv}{dx}
\end{align*}

The key is to realize that $\frac{du}{dx}\frac{dv}{dx}$ is basically zero.  This is because everything there is approaching zero - $du$, $dv$, and $dx$.  You basically have two close-to-zero fractions multiplied by each other, which is effectively zero (think .000000001 * .00000000000001).  Therefore, the final result is:
\begin{equation*}
y' = u\frac{dv}{dx} + v\frac{du}{dx} = uv' + vu'
\end{equation*}

which is the product rule as we know it.  Using this as a base, we can prove the quotient rule:

\begin{align*}
y &= \frac{u(x)}{v(x)} \\
&= uv^{-1} \\
y' &= u\left(-v^{-2}(v')\right) + v^{-1}u' \\
&= \frac{1}{v^2}\left(-uv' + vu'\right) \\
&= \frac{vu' - uv'}{v^2}
\end{align*}

which is the quotient rule as we know it.  I almost never use the quotient rule, opting instead to do a product rule like this proof.

\subsection{Trigonometric Functions}

Now that I've proved some of the more fundamental derivatives, I will move on to the trigonometric identities.  Let's try to do $\sin{x}$ first:

\begin{align*}
y &= \sin{x} \\
y' &= \displaystyle \lim_{\Delta x \to 0} \frac{\sin{\left(x + \Delta x\right)} - \sin{x}}{\Delta x} \\
&= \displaystyle \lim_{\Delta x \to 0} \frac{2\cos{\left(\frac{1}{2}\left(x + \Delta x + x\right)\right)}\sin{\left(\frac{1}{2}\left(x + \Delta x - x\right)\right)}}{\Delta x} \\
&= \displaystyle \lim_{\Delta x \to 0} \frac{2\cos{\left(x + \frac{\Delta x}{2}\right)}\sin{\frac{\Delta x}{2}}}{\Delta x} \\
&= \displaystyle \lim_{\Delta x \to 0} \frac{\cos{\left(x + \frac{\Delta x}{2}\right)}\sin{\frac{\Delta x}{2}}}{\frac{\Delta x}{2}} \\
\end{align*}

Now, at this point, we appear to be stuck.  However, there is a proof that shows that $\displaystyle \lim_{x \to 0}\frac{\sin{x}}{x} = 1$ (the proof is way too long and complicated to show here - it's available at \url{http://www.themathpage.com/acalc/sine.htm}).  Therefore, our limit becomes this:

\begin{align*}
y' &= \displaystyle \lim_{\Delta x \to 0}\cos{\left(x + \frac{\Delta x}{2}\right)} \\
&= \cos{x}
\end{align*}

There's the infamous identity!  Let's try to do the same thing for $\cos{x}$.  Now that we've established the derivative of $\sin{x}$, however, things get \emph{much} easier.

\begin{align*}
y &= \cos{x} \\
&= \sin{\left(\frac{\pi}{2} - x\right)} \\
y' &= \cos{\left(\frac{\pi}{2} - x\right)}\left(-1\right) \\
&= -\sin{x}
\end{align*}

I used the fact that $\cos{x} = \sin{\left(\frac{\pi}{2} - x\right)}$ and $\sin{x} = \cos{\left(\frac{\pi}{2} - x\right)}$.  Now that we have the derivatives of both $\sin{x}$ and $\cos{x}$, we can find the derivative of $\tan{x} = \frac{\sin{x}}{\cos{x}}$.

\begin{align*}
y &= \tan{x} \\
&= \sin{x}\cos{x}^{-1} \\
y' &= \sin{x}\left(\cos{x}^{-2}\sin{x}\right) + \cos{x}^{-1}\cos{x} \\
&= \sin{x}^2\sec{x}^2 + \cos{x}^2\sec{x}^2 \\
&= \sec{x}^2\left(\sin{x}^2 + \cos{x}^2\right) \\
&= \sec{x}^2(1) \\
&= \sec{x}^2
\end{align*}

Note that we used the trigonometric identity $\sin{x}^2 + \cos{x}^2 = 1$ to simplify.  Now let's quickly prove the derivatives of $\sec{x}$, $\csc{x}$, and $\cot{x}$.

\begin{align*}
y &= \csc{x} \\
&= \frac{1}{\sin{x}} \\
&= \sin{x}^{-1} \\
y' &= -\sin{x}^{-2}\cos{x} \\
&= -\frac{\cos{x}}{\sin{x}^2} \\
&= -\frac{\cos{x}}{\sin{x}}\frac{1}{\sin{x}} \\
&= -\cot{x}\csc{x}
\end{align*}

\begin{align*}
y &= \sec{x} \\
&= \frac{1}{\cos{x}} \\
&= \cos{x}^{-1} \\
y' &= -\cos{x}^{-2}\left(-\sin{x}\right) \\
&= \frac{\sin{x}}{\cos{x}^2} \\
&= \frac{\sin{x}}{\cos{x}}\frac{1}{\cos{x}} \\
&= \tan{x}\sec{x}
\end{align*}

\begin{align*}
y &= \cot{x} \\
&= \frac{\cos{x}}{\sin{x}} \\
y' &= \frac{\sin{x}\left(-\sin{x}\right) - \cos{x}\sin{x}}{\sin{x}^2} \\
&= \frac{-\left(\sin{x}^2 + \cos{x}^2\right)}{\sin{x}^2} \\
&= -\csc{x}^2
\end{align*}

Note that these derivatives are \emph{not} hard to derive (no pun intended).  With these proofs you will never have to worry whether the derivative of $\csc{x}$ is $-\csc{x}\cot{x}$ or $\csc{x}\cot{x}$ - you can simply do the derivation again \ldots it only takes a minute and prevents mistakes like the above.

Let's now move on to \emph{interpreting} the derivative.  Many times, you want to find the maximum or minimum of a function (to figure out where the minimum cost is, for example).  It's quite tedious to guess and check values, so there \emph{must} be a faster way!  Well, there is.

Consider what happens when you throw a ball [straight] up in the air.  It flies up, almost hangs in the air for a moment, and comes back down.  Now think about where the ball reached its maximum height.  It was where it almost hung in the air, right?  Why is that?  Well, if there is to be a maximum height, the ball will have to be going one way (+) before the maximum and the other way (-) after the maximum.  However, to switch from going up (+) to going down (-), it has got to stop!  That is, it's not physically possible for the ball to go one way and then suddenly reverse direction without stopping.  This applies to functions just as much as balls.  That is, to go from increasing to decreasing, a function has got to ``stop'' for a moment.  This ``stop'' is what we call the maximum.  Similarly, a function has got to ``stop'' while switching from decreasing to increasing.  This ``stop'' we call a minimum.  Now how is this useful?  Well, the derivative of position gives us velocity (see the next section for an explanation if you're not already familiar with this concept).  So, if we take the derivative of the height of the ball with respect to time, we'll have a function for the vertical velocity of the ball with respect to time.  We also know that the ball's height has a maximum when the ball stops in midair.  Well, at that point, the velocity is zero, isn't it?  So if we can find where the derivative is zero, we can find the minima and maxima.  \emph{This is a very important concept}.

For example, let's find the maximum of a parabola:

\begin{align*}
y &= 4x^2 \\
y' &= 8x = 0 \\
x &= 0 \\
\end{align*}

Now that's not enough.  How do we know if the point is a maximum or minimum?  We need to see whether it was increasing or decreasing before the \textbf{critical point} and whether it was decreasing or increasing after the \textbf{critical point}.  Let's plug in $x = -1$ into the derivative equation.  We get -8, which tells us the derivative was negative before the critical point.  Now we plug in $x=1$ into the derivative equation.  We get 8, which tells us the derivative is positive after the critical point.  Going from decreasing to increasing means the critical point is a minimum.

\section{Application of Derivatives}

Now we have these things called derivatives.  What can we use them for?  The answer is that you can use them for \emph{many} things.  For instance, if you know a function in terms of time for the position of a car, you can find the velocity $v$ at any time $t$.  Not only that, you can find the acceleration $a$ at any time $t$.  How?  By taking the derivative.  Let's try to understand why the derivative of position is velocity.  The position function is in terms of time $t$, in seconds, and the position itself, $x$, is in meters.  Now if we differentiate with respect to time, we'll get meters/second.  Why?  Think about what the derivative gets you.  It gives you how fast the dependent variable is changing with respect to the independent variable because it is the \emph{slope} of the function at a certain point.  Therefore, the units on the top of the slope equation will be meters, and the units on the bottom will be seconds, hence meters/second.  The same can be said for the derivative of velocity, acceleration, which has units meters/$\textrm{second}^2$.

Now another application of derivatives is related rates.  That is, given that this thing is changing at this rate and this other thing is changing at an unknown rate, find the unknown rate if the two things are related by this equation.  Many will also ask you to find the rate of change at a certain point (usually time).  How do you do these problems?  Let me give an example:

\begin{quotation}
A 5 foot ladder is leaning against a wall and is sliding down the wall at a rate of 10 feet per second.  Find the rate at which the foot is moving away from the wall when the foot of the ladder is 3 feet from the wall.
\end{quotation}

Now you say ``Ahhhhh!!!  I can't solve this!!!  They haven't given me anything!!!''.  Not to worry!  Let's look at what they \emph{have} given us.  Since the ladder is leaning against a wall, it forms a triangle with the wall and the ground.  \emph{This is crucial}.  We can then say that $25 = x^2 + y^2$, where $x$ and $y$ are the measures of the two other sides of the triangle.  The measure parallel to the ground is $x$.  Then, $0 = 2x*\frac{dx}{dt} + 2y*\frac{dy}{dt}$, because we're differentiating with respect to $t$.  We know that, at this instant, $x=3$.  Then, because the ladder is a fixed length, 5 feet, we can figure out from the Pythagorean Formula that $y=4$.  Then, plugging these values in and solving for $\frac{dy}{dt}$, we get $0 = 60 + 8\frac{dy}{dt}$ and $\frac{dy}{dt} = -\frac{15}{2}$.  Let's do a sanity check (just to be sure).  Since the foot of the ladder is moving away from the wall, the top of the ladder \emph{must} be moving down, which is confirmed by our answer.

As far as I can tell, there aren't many more ``applications'' per se that I know of (although I might be missing out on some stuff - this \emph{is} a guide after all, not a textbook!).

\section{The Concept of the Integral}

There were two big problems in Math before Calculus came along.  One was how to find the rate of change of something, which was solved by the concept of the derivative.  The other was how to find the area under the curve.  This problem was solved by concept of the integral.  Now what exactly is the integral?  We'll get there.  First let's consider a simple function such as $y=t^2$.  Let's say that function represents the velocity of an object at a certain time $t$.  Now how do we find out what vertical distance the object has traveled in, let's say 5 seconds.  Let's look at the units for the axes.  The x-axis (or really, t-axis) is in seconds (because it's a time).  The y-axis is in m/s (because it's a velocity).  Now let's take the area of a certain rectangle (just to see what happens).  How do we construct a rectangle?  Well, we need 2 $t$ values and 1 $y$ value (otherwise it will become a trapezoid).  So let's take the area of the rectangle $t=1$, $t=2$, and $y=2.25$ (I took the midpoint of the two $t$ values and found its $y$ value - kind of a compromise).  When we take the area of that, we get $2.25$.  \emph{But what are the units?}  Let's take a look.  The unit for the width is seconds.  The unit for the height/length is meters/second.  When we multiply those together, \emph{we get meters}.  So by taking that area, we can find the approximate displacement of the ball between those two times (i.e. how much it moved up or down).  Why is it approximate?  Because the only way to get an exact area is to do an integral, which is coming up soon.  Now that we have the concept, let's try to get the \emph{exact} area under the curve.  Why don't we try breaking it up into a lot of tiny rectangles?  That way, we can closely follow the curve, providing greater accuracy.  Let's look again at the general formula for that area we calculated.  It was width $\times$ height, which is $y \times \Delta t$.  But remember that that was just \emph{one} rectangle!  We need to do this for as many as possible to fit the curve as much as possible, thereby providing as accurate a result as possible.  In order to do this, why don't we just make all of the $\Delta t$ into $dt$?  That is, make it a differential $t$ (infinitesimally small).  Now we have $y \times dt$.  But we need to add up all of those on some interval $[t_i,t_f]$.  So that becomes $\displaystyle\sum\limits_{t=t_i}^{t_f} f(t)dt$ where $y$ has been replaced by $f(t)$.  Of course, mathematicians had to invent shorthand for this (it was too tedious for them to write every time) so they made up a symbol and called it the \emph{integral}.  The previous sum can now be written as $\int_{t_i}^{t_f}f(t)dt$ and is called ``The integral of $f(t)$ from $t_i$ to $t_f$ with respect to $t$''.  Now how do we evaluate them?  There's a huge catch: if you know the formula, you can.  Otherwise, you need to consult a computer/calculator which will do numerical approximations.  Now what are all these formulae? I have below a list of formulae for integrals.  Most follow from their derivative counterparts, but some I will explicitly prove.

\begin{enumerate}
\item $\int e^u du = e^u + C$
\item $\int \frac{du}{u} = \ln{u} + C$
\item $\int b^u du = \frac{b^u}{\ln{u}} + C$
\item $\int u^n du = \frac{u^{n+1}}{n+1} + C$
\item $\int \cos{u} \,du = \sin{u} + C$
\item $\int \sin{u} \,du = -\cos{u} + C$
\item $\int \tan{u} \,du = -\ln{\cos{u}} + C$
\item $\int \cot{u} \,du = \ln{\sin{x}} + C$
\item $\int \csc{u} \,du = -\ln{\left|\csc{u} + \cot{u}\right|} + C$
\item $\int \sec{u} \,du = \ln{\left|\sec{u} + \tan{u}\right|} + C$
\item $\int \sec{u}\tan{u}\,du = \sec{u} + C$
\item $\int \csc{u}\cot{u}\,du = \csc{u} + C$
\item $\int \sec{u}^2\,du = \tan{u} + C$
\item $\int \csc{u}^2\,du = -\cot{u} + C$
\item $\int \arcsin{u}\,du = u\arcsin{u} + \sqrt{1-u^2} + C$
\item $\int \arccos{u}\,du = u\arccos{u} - \sqrt{1-u^2} + C$
\item $\int \arctan{u}\,du = u\arctan{u} - \frac{1}{2}\ln{1+u^2} + C$
\item $\int \frac{du}{\sqrt{1-u^2}} = \arcsin{u} + C$
\item $\int \frac{du}{u\sqrt{u^2 - 1}} = \textrm{arcsec}\,{\left|u\right|} + C$
\end{enumerate}

Let's figure out how some of these integrals came to be.

\begin{align*}
y &= \tan{x} \\
\int y\,dx &= \int \tan{x}\,dx \\
&= \int \frac{\sin{x}}{\cos{x}}\,dx \\
\textrm{Let } u &= \cos{x} \\
\int y\,dx &= \int \frac{-du}{u} \\
&= -\ln{u} + C \\
&= -\ln{\left|cos{x}\right|} + C
\end{align*}

\begin{align*}
y &= \cot{x} \\
\int y\,dx &= \int \cot{x}\,dx \\
&= \int \frac{\cos{x}}{\sin{x}}\,dx \\
\textrm{Let } u &= \sin{x} \\
\int y\,dx &= \int\frac{du}{u} \\
&= \ln{u} + C \\
&= \ln{\sin{x}} + C
\end{align*}

\begin{align*}
y &= b^x \\
\int y\,dx &= \int b^x\,dx \\
&= \int e^{\ln{b^x}}\,dx \\
&= \int e^{x\ln{b}}\,dx \\
\textrm{Let }u &= x\ln{b} \\
\textrm{Then }du &= \ln{b}\,dx \\
\int y\,dx &= \frac{1}{\ln{b}}\int e^u\,du \\
&= \frac{e^u}{\ln{b}} + C \\
&= \frac{b^x}{\ln{b}} + C
\end{align*}

\begin{align*}
y &= \sec{x} \\
\int y\,dx &= \int \sec{x}\,dx \\
&= \int \sec{x}\frac{\sec{x} + \tan{x}}{\sec{x} + \tan{x}}\,dx \\
\textrm{Let }u &= \sec{x} + \tan{x} \\
\textrm{Then }du &= \sec{x}\tan{x} + \sec^2{x} \\
\int y\,dx &= \int \frac{du}{u} \\
&= \ln{\left|u\right|} + C \\
&= \ln{\left|\sec{x} + \tan{x}\right|} + C
\end{align*}

\begin{align*}
y &= \csc{x} \\
\int y\,dx &= \int \csc{x}\,dx \\
&= \int \csc{x}\frac{\csc{x} + \cot{x}}{\csc{x} + \cot{x}}\,dx \\
\textrm{Let }u &= \csc{x} + \cot{x} \\
\textrm{Then }du &= -\csc{x}\cot{x} - \csc^2{x} \\
\int y\,dx &= \int \frac{-du}{u} \\
&= -\ln{\left|\csc{x} + \cot{x}\right|} + C
\end{align*}

\begin{align*}
y &= \arcsin{x} \\
\int y\,dx &= \int \arcsin{x}\,dx
\end{align*}

Before I go any further, I need to introduce the rule of Integration by Parts.  Integration by Parts says that $\int u\,dv = uv - \int v\,du$.  This means that if we can separate the integral above into two parts, $u$ and $dv$, then we can use Integration by Parts to solve it.
\[
\begin{array}{c c}
u = \arcsin{x} & v = x \\
du = \frac{1}{\sqrt{1-x^2}} & dv = dx
\end{array}
\]
\begin{align*}
\int u\,dv &= uv - \int v\,du \\
&= x\arcsin{x} - \int \frac{x}{\sqrt{1-x^2}}\,dx \\
&= x\arcsin{x} - \frac{-1}{2}\left(\frac{\sqrt{1-x^2}}{\frac{1}{2}}\right) + C \\
&= \sqrt{1-x^2} + x\arcsin{x} + C
\end{align*}



\section{Applications of Integrals}

\section{Sequences and Series}

\section{Maclaurin and Taylor polynomials/series}

\section{Revisiting limits}

\section{Computing Error}
\end{document}