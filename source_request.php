<!DOCTYPE html5>
<?php include('hsts.php') ?>
<html lang=en>
  <head>
    <?php
       if(($_POST['firstname'] == "") || ($_POST['lastname'] == "") || ($_POST['email'] == "") || ($_POST['reason'] == "")) {
         print '<title>Error!</title>';
         print '<link href="main.css" type="text/css" rel="stylesheet"/>';
         print '<meta http-equiv="REFRESH" content="2;url=contact.php">';
         }
       else {
         print '<title>Sent!</title>';
         print '<link href="main.css" type="text/css" rel="stylesheet"/>';
         print '<meta http-equiv="REFRESH" content="2;url=programs/index.php">';
       }
    ?>
    <?php include('base.php') ?>
  </head>
  <body>
    <?php include('navigation.php') ?>
    <div id="content">
      <?php
         if(($_POST['firstname'] == "") || ($_POST['lastname'] == "") || ($_POST['email'] == "") || ($_POST['reason'] == "")) {
         print "Error with the form - one or more fields left blank.";
         die();
         }
         $my_email = 'chiraag@chiraagnataraj.us';
         $subject = 'Musik Source Request';
         $first_name = $_POST['firstname'];
         $last_name = $_POST['lastname'];
         $email = $_POST['email'];
         $reason = $_POST['reason'];
         $message = ($first_name . " ") . ($last_name . "\n") . ($email . "\n") . $reason;
         $message = wordwrap($message, 80);
         mail($my_email, $subject, $message);
         print "Message sent!"
      ?>
      <hr width="100%" align="center" />
      <?php include('footer.html') ?>
    </div>
  </body>
</html>
