<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Gun Deaths and What To Do About Them</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>
	Firefox Tips and Tricks
      </h1>
      <p>
	Firefox 57 will invalidate many of the existing useful extensions (like FaviconizeTab, which was a personal favorite). Some will see replacements. Others, however, can be replicated by existing customization options, albeit well-hidden ones.
      </p>
      <h2>
	Setup
      </h2>
      <p>
	In order for many of these tricks to be useful, you will need to create a file called <kbd>userChrome.css</kbd>. First, find your profile folder by going to <kbd>about:support</kbd> and clicking on <kbd>Open Directory</kbd> next to <strong>Profile Directory</strong> under <strong>Application Basics</strong>. Then, navigate to the <kbd>chrome</kbd> directory and copy the <kbd>userChrome-example.css</kbd> file to <kbd>userChrome.css</kbd>. Now open the file in your favorite text editor.
      </p>
      <h2>
	Reducing the width of your non-pinned tabs
      </h2>
      <p>
	Before Firefox 57, there were some addons that would reduce the size of your tabs (e.g. FaviconizeTab). If you want to replicate this, add this at the bottom of <kbd>userChrome.css</kbd>:
      </p>
      <textarea name="Faviconize Tabs" class="code" rows="4" cols="26" readonly="readonly">
.tabbrowser-tab:not([pinned])
{
   max-width: 40px !important;
   min-width: 40px !important;
   
}
      </textarea>
      <h2>
	Hiding the close button on non-pinned tabs
      </h2>
      <p>
	With all the different ways to close tabs, from keyboard shortcuts to mouse shortcuts, who needs the close button to appear on the tab?
      </p>
      <textarea name="Remove Close Button" class="code" rows="4" cols="26" readonly="readonly">
.tabbrowser-tab .tab-close-button
{
   display:none!important;
}
      </textarea>
      <h2>
	Hide back and forward buttons
      </h2>
      <p>
	If you have other ways of navigating the web and don't need to see the back and forward buttons (that is, they're just taking up valuable screen real estate), you can hide them like this:
      </p>
      <textarea name="Remove Close Button" class="code" rows="4" cols="26" readonly="readonly">
#back-button
{
   display: none;
}

#forward-button
{
   display: none;
}
      </textarea>
      <h2>
	Disabling Pocket completely
      </h2>
      <p>
	If you prefer to not use Pocket at all and wipe all traces of it from your browser, simply go to <kbd>about:config</kbd> and set <kbd>extensions.pocket.enabled</kbd> to <kbd>false</kbd>.
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
