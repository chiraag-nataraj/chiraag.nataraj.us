<!DOCTYPE html>
<?php include('../hsts.php') ?>
<html lang=en>
  <head>
    <title>Research Reports</title>
    <meta charset="utf-8" />
    <link href="../main.css" type="text/css" rel="stylesheet" />
    <?php include('../base.php') ?>
  </head>
  <body>
    <?php include('../navigation.php') ?>
    <div id="content">
      <h1>Research Reports</h1>
      <h2>Undergraduate Thesis &mdash; <a href="reports/undergrad-thesis.pdf" target="_blank" type="applications/pdf">Full thesis</a></h2>
      <p>
	In order to study the fracture mechanics of heterogeneous materials as an avenue for attempting to characterize heterogeneous materials, it is useful to utilize alternative measures which are easier to measure while providing an understanding of the underlying phenomena. The behavior of heterogeneous materials under peeling action will be explored as a proxy for understanding fracture mechanics.
      </p>
      <p>
	The first objective is to perform a peeling experiment to investigate how different patterns on a substrate affect adhesion. Several different patterns will be tested and the force required to peel the tape off at constant velocity will be measured. I will attempt to mathematically model the results from that experiment as well as design further experiments to test hypotheses about phenomena that may arise.
      </p>
      <p>
	The second objective is to computationally model the peel front during the peeling process to track how it changes. This will thus give an idea of how the heterogeneities affect the peel front.
      </p>
      <h2>Optimizing Spinal Cord Injury Therapy &mdash; <a href="reports/kinect-report.pdf" target="_blank" type="applications/pdf">Full report</a></h2>
      <p>
	The objective of this research is to utilize depth sensor technology to track body motion. Currently, many algorithms exist to do &mdash; one of the most popular ones is SCAPE, which has two stages. The first stage is the initialization phase, whereby the generic human model is modified to resemble the actual human. The second stage is the tracking phase, whereby the generated model is then used along with keypoints to track the motion of the person. The key contribution of this research is in the first phase. Normally, the initialization is done by hand by selecting several keypoints on both the captured image and the model. This research seeks to automate that process using a combination of edge detection and point-to-point correspondence given that the initial pose is roughly known.
      </p>
      <h2>Hydraulics for Micro-Robotic Actuation &mdash; <a href="reports/hydraulics-report.pdf" target="_blank" type="applications/pdf">Full report</a></h2>
      <p>
	The objective of the proposed student research is to investigate hydraulic actuation for small and micro robotic systems, and develop control algorithms for their autonomous operation.  The objective is to implement this idea in a test system to prove the viability of the concept.  In order to ensure that the pump is as efficient as possible, equations and parameters are used in order to optimize the efficiency and force/pressure output of the pump.  Furthermore, parametrized CAD drawings of the different parts of the pump are utilized in a programmatic manner to facilitate changing parameters.  Once the pump has been optimized and fully developed, it will be used to power an actuator which can trigger an action such as e.g. an arm bringing down a hammer.  For the case of an arm and hammer, pistons can be placed both above and below the joint to allow the arm to be raised and lowered.
      </p>
      <h2>Modeling of Negative Index Metamaterials for &#8220;Hide-in-Plain-Sight&#8221; &mdash; <a href="reports/nims-report.pdf" target="_blank" type="applications/pdf">Full report</a></h2>
      <p>
	The objective of this research was to investigate the possibility of creating &#8220;hide-in-plain-sight&#8221; effects using negative index metamaterials (NIMs). The objective was to determine if and how it is possible to use NIMs in order to conceal or disguise an object.  In order to investigate these questions, POV-Ray, a raytracing software, was used to model negative index of refraction metamaterials in various configurations. Investigations were conducted based upon varying geometry and gradients of index of refraction to study the effect on an observer's perception of an object enclosed in metamaterial. Results indicate that reflectance and transmittance of a given object are the same whether the index of refraction is positive or negative. Furthermore, using certain combinations of metamaterials and ordinary materials, a variety of optical effects can be achieved. Finally, at certain viewing angles, NIMs are not necessary in order to produce &#8220;hide-in-plain-sight&#8221; effects --- positive index of refraction metamaterials would also work. These results portend that &#8220;hide-in-plain-sight&#8221; may not be far off in the future. This research was presented at the 2012 Annual Southern California Conference for Undergraduate Research.
      </p>
      <h2>Complexity in Nonlinear Dynamic Systems &mdash; <a href="reports/chaos-report.pdf" target="_blank" type="applications/pdf">Full report</a></h2>
      <p>
	The objective was to create an auditory representation and interpretation of chaos, where a pattern called a strange attractor was reproduced in the music. A strange attractor is a particular manifestation of chaos in which one may not know the exact coordinates, but knows the coordinates will fall around a certain pattern. Chaos is a complex, dynamic system which is often analyzed using conventional, quantitative techniques. With my background and passion for music I pursued a research venture into the beautiful, but alien (to the world of math), realm of music in order to completely understand the nature of chaos. By adding this new, richer dimension into the mix, my hope is that one can immediately pick out certain aspects of chaos which are not immediately visible from conventional graphs. This research was presented at the Carderock Division of the Naval Surface Warfare Center.
      </p>
      <h2>Swarm Robotics &mdash; <a href="reports/swarm-report.pdf" target="_blank" type="text/html">Paper</a></h2>
      <p>
	The objective was to implement swarm robotics using particle swarm optimization, where a group of robots uses localized computations to achieve a group task in an optimal fashion. This project was presented at two conferences, one of which was a national ASEE conference [Chiraag Nataraj, Sanjeev Reddy, Mark Woods, B. Samanta, and C. Nataraj, 2010, Swarm Robotics: A Research Project With High School Students As Active Participants,'' American Society of Engineering Education Annual Conference, Louisville, KY, June 2010, Paper No. AC 2010-1655.]
      </p>
      <hr />
      <?php include('../footer.html') ?>
    </div>
  </body>
</html>
